<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\FileSystem;


class Volatile
{
    protected $_current_directory;

    public function __construct()
    {
        $this->_current_directory = Directory::root();
    }


    public function cd($path)
    {
        $path = $this->asPath($path);
        if ($path->fileExists())
            $this->_current_directory = $path->asDirectory();

        return $this;
    }


    public function mkdir($path)
    {
        $this->asPath($path)->mkdir();
        return $this;
    }


    public function touch($path)
    {
        if ($directory = $this->asPath(dirname($path))->asDirectory())
            $directory->touch(basename($path));

        return $this;
    }


    public function fileExists($path)
    {
        return $this->asPath($path)->fileExists();
    }


    public function filePutContents($path, $data)
    {
        $this->touch($path);
        $this->entryAt($path)->putContents($data);
        return $this;
    }


    public function fileGetContents($path)
    {
        return ($file = $this->asPath($path)->asEntry())
            ? $file->getContents()
            : null;
    }


    public function fileGetSize($path)
    {
        return ($file = $this->asPath($path)->asEntry())
            ? $file->getSize()
            : 0;
    }


    public function rm($path)
    {
        $this->asPath($path)->rm();
        return $this;
    }


    public function asPath($path)
    {
        return (new Path($path, $this->_current_directory));
    }


    public function entryAt($path)
    {
        return $this->asPath($path)->asEntry();
    }


    public function directoryNamesAt($path)
    {
        return $this->_nodeNamesOfTypeAt(
            function ($entry) { return $entry->isDirectory(); },
            $path
        );
    }


    public function fileNamesAt($path) {
        return $this->_nodeNamesOfTypeAt(
            function ($entry) { return $entry->isFile(); },
            $path
        );
    }


    protected function _nodeNamesOfTypeAt($type_closure, $path)
    {
        $collection = $this->entryAt($path)->select($type_closure);

        array_walk($collection,
                   function (&$entry) {
                       $entry = $entry->getName();
                   });

        return $collection;
    }
}




class Path
{
    protected
        $_path,
        $_directory,
        $_parts;


    public function __construct($path, $directory)
    {
        $this->_path = $path;
        $this->_parts = array_filter(explode('/', $path), [$this, 'isValidPathPart']);
        $this->_directory = $directory;
    }


    protected function isValidPathPart($part)
    {
        return !in_array($part, ['', '.']);
    }


    public function isAbsolute()
    {
        return  $this->_path && ($this->_path[0] === '/');
    }


    public function isRoot()
    {
        return $this->_path === '/';
    }


    public function getRootDirectory()
    {
        return $this->_directory->getRoot();
    }


    public function mkdir()
    {
        $parts = $this->_parts;
        $subdir = $this->isAbsolute()
            ? $this->getRootDirectory()
            : $this->_directory->ensureDirectory(array_shift($parts));

        if ($parts)
            $subdir->relativePath(implode('/', $parts))->mkdir();

        return $this;
    }


    public function fileExists()
    {
        if ($this->isRoot())
            return true;

        if (!$parts = $this->_parts)
            return true;

        $subdir = $this->isAbsolute()
            ? $this->getRootDirectory()
            : $this->_directory->getSubDirectoryNamed(array_shift($parts));

        if (!$subdir)
            return false;

        return $subdir->relativePath(implode('/', $parts))->fileExists();
    }


    public function rm()
    {
        $this->asEntry()->rm();
    }


    public function asEntry()
    {
        return $this->asDirectory();
    }


    public function asDirectory()
    {
        if ($this->isRoot())
            return $this->getRootDirectory();

        if (!$parts = $this->_parts)
            return $this->_directory;

        $subdir = $this->isAbsolute()
            ? $this->getRootDirectory()
            : $this->_directory->getSubDirectoryNamed(array_shift($parts));

        if (!$subdir)
            return null;

        return $subdir->relativePath(implode('/', $parts))->asDirectory();
    }
}




abstract class Entry
{
    protected
        $_name,
        $_parent;

    public function __construct($name, $parent)
    {
        $this->_name = $name;
        $this->_parent = $parent;
    }


    public function relativePath($path)
    {
        return new Path($path, $this);
    }


    public function getRoot()
    {
        return $this->_parent ? $this->_parent->getRoot() : $this;
    }


    public function rm()
    {
        $this->_parent->removeEntryNamed($this->_name);
        return $this;
    }


    public function getName()
    {
        return $this->_name;
    }


    abstract function isDirectory();
}




class Directory extends Entry
{
    protected
        $_entries = [];


    public static function root()
    {
        return new self('', null);
    }


    public function mkdir($name)
    {
        return $this->_entries[$name] = new self($name, $this);
    }


    public function fileExists($name)
    {
        return in_array($name, array_keys($this->_entries));
    }


    public function getSubdirectoryNamed($name)
    {
        return $this->fileExists($name) ? $this->_entries[$name] : null;
    }


    public function ensureDirectory($name)
    {
        return $this->fileExists($name)
            ? $this->getSubdirectoryNamed($name)
            : $this->mkdir($name);
    }


    public function touch($name)
    {
        $this->_entries [$name]= new File($name, $this);
        return $this;
    }


    public function removeEntryNamed($name)
    {
        unset($this->_entries[$name]);
        return $this;
    }


    public function collect($a_block)
    {
        $collection = array_values($this->_entries);
        return array_map($a_block, $collection);
    }


    public function select($a_block)
    {
        return array_filter($this->_entries, $a_block);
    }


    public function isDirectory()
    {
        return true;
    }
}




class File extends Entry
{
    protected $_contents = '';

    public function isDirectory()
    {
        return false;
    }


    public function isFile()
    {
        return true;
    }


    public function putContents($contents)
    {
        $this->_contents = $contents;
    }


    public function getContents()
    {
        return $this->_contents;
    }


    public function getSize()
    {
        return strlen($this->_contents);
    }
}
