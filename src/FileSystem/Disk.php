<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\FileSystem;


class Disk
{
    public function fileExists($path)
    {
        return file_exists($path);
    }


    public function fileGetContents($path)
    {
        return file_get_contents($path);
    }


    public function fileGetSize($path)
    {
        return filesize($path);
    }


    public function filePutContents($path, $data)
    {
        file_put_contents($path, $data);
        return $this;
    }


    public function directoryNamesAt($path)
    {
        if (!file_exists($path))
            return [];

        $dirs = [];
        foreach (new \DirectoryIterator($path) as $entry) {
            if ($entry->isDir() && !$entry->isDot())
                $dirs[$entry->getFilename()] = $entry->getFilename();
        }

        asort($dirs);
        return $dirs;
    }


    public function fileNamesAt($path)
    {
        if (!file_exists($path))
            return [];

        $files = [];
        foreach (new \DirectoryIterator($path) as $entry) {
            if ($entry->isFile() && !$entry->isDot())
                $files[$entry->getFilename()] = $entry->getFilename();
        }

        asort($files);
        return $files;
    }
}
