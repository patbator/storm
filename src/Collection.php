<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2018- Patrick Barroca and contributors, see CONTRIBUTORS.txt
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm;


class Collection extends \ArrayObject
{
    public function addAll($collection)
    {
        foreach($collection as $element)
            $this->append($element);

        return $collection;
    }


    public function newInstance($elements) : self
    {
        $classname = get_class($this);
        return new $classname($elements);
    }


    public function collect($closure) : self
    {
        return $this->newInstance(array_map($closure,
                                            (array)$this));
    }


    public function detect($closure)
    {
        foreach($this as $value)
            if ($closure($value))
                return $value;
    }


    public function select($closure) : self
    {
        return $this->newInstance(array_values(array_filter((array)$this, $closure)));
    }


    public function reject($closure) : self
    {
        return $this->select(fn($e) => !$closure($e));
    }


    public function eachDo($closure) : self
    {
        foreach($this as $value)
            $closure($value);

        return $this;
    }


    public function injectInto($value, $closure)
    {
        $next_value = $value;
        $this->eachDo(function($each) use(&$next_value, $closure) {
            $next_value = $closure($next_value, $each);
        });

        return $next_value;
    }


    public function isEmpty() : bool
    {
        return $this->count() === 0;
    }


    public function first()
    {
        return $this->detect(fn() => true);
    }


    public function last()
    {
        return ($values = $this->getArrayCopy())
            ? end($values)
            : null;
    }


    public function includes($object) : bool
    {
        return in_array($object, $this->getArrayCopy());
    }


    public function without($object) : self
    {
        return $this->reject(fn($each) => $each === $object);
    }
}
