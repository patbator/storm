<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Cache;

use Storm\Cache\Serialize;


class Cache
{
    protected
        $_enabled = true,
        $_cache,
        $_serialization_strategy;

    protected static
        $_default_cache,
        $_seed = '',
        $_real_seed;


    public static function setDefaultCache($cache)
    {
        static::$_default_cache = $cache;
    }


    public static function setSeed($seed)
    {
        static::$_seed = str_replace('-', '_', $seed);
        static::$_real_seed = null;
    }


    public static function beVolatile()
    {
        static::setDefaultCache(new VolatileCache());
    }


    public static function getDefaultCache()
    {
        if(!isset(static::$_default_cache))
            static::$_default_cache = new NullCache();

        return static::$_default_cache;
    }


    public function __construct()
    {
        $this->useNativeSerialization();
    }


    public function useNativeSerialization()
    {
        $this->_serialization_strategy = new Serialize\NativeStrategy();
        return $this;
    }


    public function useImplodeExplodeSerialization()
    {
        $this->_serialization_strategy = new Serialize\ImplodeExplodeStrategy();
        return $this;
    }


    public function disableSerialization()
    {
        $this->_serialization_strategy = new Serialize\NullStrategy();
        return $this;
    }


    public function setEnabled($enabled)
    {
        $this->_enabled = $enabled;
        return $this;
    }


    public function setCache($cache)
    {
        $this->_cache = $cache;
        return $this;
    }


    public function memoize($key, $callback)
    {
        if(!$this->_enabled)
            return $callback();

        if ($this->test($key))
            return $this->load($key);

        $data = $callback();
        $this->save($data, $key);

        return $data;
    }


    public function getCache()
    {
        if(!isset($this->_cache))
            $this->_cache = static::getDefaultCache();

        return $this->_cache;
    }


    public function serialize($data)
    {
        return $this->_serialization_strategy->serialize($data);
    }


    public function unserialize($data)
    {
        return $this->_serialization_strategy->unserialize(trim($data));
    }


    public function load($key)
    {
        return $this->unserialize($this->getCache()->load($this->addSeedToKey($key)));
    }


    public function save($data, $key)
    {
        return $this->getCache()->save($this->serialize($data), $this->addSeedToKey($key));
    }


    public function remove($key)
    {
        return $this->getCache()->remove($this->addSeedToKey($key));
    }


    public function test($key)
    {
        return $this->getCache()->test($this->addSeedToKey($key));
    }


    public function addSeedToKey($key)
    {
        return md5(serialize([$this->_getRealSeed(), $key]));
    }


    protected function _getRealSeed()
    {
        if (static::$_real_seed)
            return static::$_real_seed;

        if (!static::$_real_seed = $this->getCache()->load(static::$_seed))
            static::$_real_seed = $this->_generateRealSeed();

        return static::$_real_seed;
    }


    protected function _generateRealSeed()
    {
        $my_real_seed = uniqid(static::$_seed . '_');
        $this->getCache()->save($my_real_seed, static::$_seed);

        return $my_real_seed;
    }


    public function clean()
    {
        static::$_real_seed = $this->_generateRealSeed();
        return $this;
    }
}
