<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

namespace Storm\Testing;

use Storm\Model\ModelAbstract;


class ObjectWrapper
{
  protected ?object $_wrapped_object;
  protected array $_call_trace = [];
  protected array $_redirections = [];
  protected bool $_is_strict = false;


  public static function on(?object $object): self
  {
    $wrapper = new self();
    return $wrapper->wrap($object);
  }


  public static function mock(string $name = '')
  {
    if (!$name) {
      $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
      $name = 'from ' . $stack[0]['file'] . ':' . $stack[0]['line'];
    }

    return self::on(new Mock($name));
  }


  public static function onLoaderOfModel(string $model_class): self
  {
    $loader = call_user_func(array($model_class, 'getLoader'));
    $wrapper = self::on($loader);
    ModelAbstract::setLoaderFor($model_class, $wrapper);
    return $wrapper;
  }


  public function beStrict(): self
  {
    $this->_is_strict = true;
    return $this;
  }


  public function getWrapper(): self
  {
    return $this;
  }


  public function wrap(?object $object): self
  {
    $this->_wrapped_object = $object;
    return $this;
  }


  public function getWrappedObject(): ?object
  {
    return $this->_wrapped_object;
  }


  protected function _raiseRedirectionNotFound(string $method, array $args): void
  {
    ob_start();
    var_dump($args);
    $dump = ob_get_contents();
    ob_end_clean();

    $message = sprintf(
      'Cannot find redirection for %s::%s(%s) %s',
      get_class($this->_wrapped_object),
      $method,
      $dump,
      ($this->_wrapped_object instanceof Mock)
        ? '[' . $this->_wrapped_object->toString() . ']'
        : ''
    );

    throw new ObjectWrapperException($message);
  }


  protected function _findRedirection(string $method, array $args): ?MethodRedirection
  {
    foreach ($this->_redirections as $redirection)
      if ($redirection->matchMethodAndArgs($method, $args))
        return $redirection;

    if ($this->_is_strict)
      $this->_raiseRedirectionNotFound($method, $args);

    foreach ($this->_redirections as $redirection)
      if ($redirection->matchMethod($method) and !$redirection->expectArgs())
        return $redirection;

    return null;
  }


  public function __call(string $method, array $args)
  {
    $this->_call_trace[$method][] = $args;

    if ($redirection = $this->_findRedirection($method, $args))
      return $redirection->getValueToAnswer($args);

    $result = call_user_func_array(
      array($this->_wrapped_object, $method),
      $args
    );

    if ($result === $this->_wrapped_object)
      return $this;

    return $result;
  }


  public function methodHasBeenCalled(string $method_name): bool
  {
    return array_key_exists($method_name, $this->_call_trace);
  }


  public function methodHasNotBeenCalled(string $method_name): bool
  {
    return !$this->methodHasBeenCalled($method_name);
  }


  public function methodHasBeenCalledWithParams(string $method_name, array $params): bool
  {
    if (!$this->methodHasBeenCalled($method_name))
      return false;

    foreach ($this->_call_trace[$method_name] as $given_params)
      if ($params == $given_params)
        return true;

    return false;
  }


  public function methodCallCount(string $method_name): int
  {
    if (!$this->methodHasBeenCalled($method_name))
      return 0;

    return count($this->_call_trace[$method_name]);
  }


  public function getAttributesForLastCallOn(string $method_name): array
  {
    return $this->getAttributesForMethodCallAt($method_name, -1);
  }


  public function getFirstAttributeForLastCallOn(string $method_name)
  {
    return $this->getAttributesForLastCallOn($method_name)[0];
  }


  /**
   * @param $index int (note: -1 mean the last one, -2 the one before the last, ...)
   */
  public function getFirstAttributeForMethodCallAt(string $method_name, int $index)
  {
    return $this->getAttributesForMethodCallAt($method_name, $index)[0];
  }


  /**
   * @param $index int (note: -1 mean the last one, -2 the one before the last, ...)
   */
  public function getAttributesForMethodCallAt(string $method_name, int $index)
  {
    if (!$this->methodHasBeenCalled($method_name))
      throw new ObjectWrapperException("Method '$method_name' has never been called");

    $index = $index >= 0 ? $index : count($this->_call_trace[$method_name]) + $index;
    return $this->_call_trace[$method_name][$index];
  }


  public function whenCalled(string $method_name): MethodRedirection
  {
    $redirection = MethodRedirection::onWrapper($this)->setMethod($method_name);
    array_unshift($this->_redirections, $redirection);
    return $redirection;
  }


  public function shouldNotBeCalled(string $method_name): MethodRedirection
  {
    return $this->whenCalled($method_name)->never();
  }


  public function clearAllRedirections(): self
  {
    $this->_redirections = [];
    $this->_call_trace = [];
    return $this;
  }
}
