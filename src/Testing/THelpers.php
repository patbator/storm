<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

namespace Storm\Testing;

use date;
use Storm\Model\ModelAbstract;

trait THelpers
{
  public function onLoaderOfModel(string $model): ObjectWrapper
  {
    return ObjectWrapper::onLoaderOfModel($model);
  }


  public function mock(): ObjectWrapper
  {
    $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
    return ObjectWrapper::mock('from ' . $stack[0]['file'] . ':' . $stack[0]['line']);
  }


  public function assertTimeStampEquals(
    $expected,
    $actual,
    string $message = "Failed asserting that two times are equal"
  ): void {
    $format = function ($ts) {
      return is_int($ts) ? date('Y-m-d H:i:s', $ts) : $ts;
    };

    $this->assertEquals($format($expected), $format($actual), $message);
  }


  public static function fixture(string $class_name, array $attributes): ModelAbstract
  {
    $class_name::beVolatile();

    $instance = $class_name::newInstanceWithId($attributes['id'], $attributes);
    $instance->assertSave();
    return $instance;
  }
}
