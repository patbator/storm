<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Testing;

use DOMNodeList;
use DOMXPath;
use DOMDocument;

/**
 * Asserter for XPath matching with namespaces capabilities
 */
class XPathXML
{
  protected array $_namespaces = [];

  public function registerNameSpace(string $prefix, string $namespaceURI): self
  {
    $this->_namespaces[$prefix] = $namespaceURI;
    return $this;
  }


  public function newXPathForXml(string $xml): DOMXPath
  {
    $document = $this->_getDocument($xml);
    $xpath = new DOMXPath($document);

    foreach ($this->_namespaces as $prefix => $uri)
      if (!$xpath->registerNamespace($prefix, $uri))
        throw new XPathException('Error registering namespace ' . $prefix);

    return $xpath;
  }


  public function assertXpathContentContains(
    string $xml,
    string $path,
    string $match,
    string $message = ''
  ): void {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $nodeList = $this->_getNodeList($xml, $path);
    $this->_assertNodeListNotEmpty($nodeList, $path, $message);
    foreach ($nodeList as $node) {
      if ('' === $match && '' === $node->nodeValue)
        return;

      if (false !== strpos($node->nodeValue, $match))
        return;
    }

    throw new XPathException($message
      . sprintf(
        'Failed asserting node denoted by %s CONTAINS content "%s"',
        $path,
        $match
      ));
  }


  public function assertNotXpathContentContains(
    string $xml,
    string $path,
    string $match,
    string $message = ''
  ): void {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $nodeList = $this->_getNodeList($xml, $path);
    $this->_assertNodeListNotEmpty($nodeList, $path, $message);
    foreach ($nodeList as $node)
      if (false !== strpos($node->nodeValue, $match))
        throw new XPathException($message
          . sprintf(
            'Failed asserting node denoted by %s DOES NOT CONTAIN content "%s"',
            $path,
            $match
          ));
  }


  public function assertXpath(string $xml, string $path, string $message = ''): void
  {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $nodeList = $this->_getNodeList($xml, $path);
    $this->_assertNodeListNotEmpty($nodeList, $path, $message);
  }


  public function assertNotXpath(string $xml, string $path, string $message = ''): void
  {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $nodeList = $this->_getNodeList($xml, $path);
    if ($nodeList->length > 0)
      throw new XPathException($message . sprintf(
        'Failed asserting that node denoted by "%s" DOES NOT EXIST',
        $path
      ));
  }


  public function assertXpathCount(
    string $xml,
    string $path,
    int $count,
    string $message = ''
  ): void {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $nodeList = $this->_getNodeList($xml, $path);
    if ($nodeList->length != (int)$count)
      throw new XPathException($message . sprintf(
        'Failed asserting that node denoted by "%s" APPEARS EXACTLY %s TIMES',
        $path,
        $count
      ));
  }


  public function assertXmlVersion(string $xml, string $version, string $message = ''): void
  {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $document = $this->_getDocument($xml);
    if ($document->xmlVersion != $version)
      throw new XPathException($message . sprintf(
        'Failed asserting that XML version of document "%s" EQUALS "%s"',
        $document->xmlVersion,
        $version
      ));
  }


  public function assertXmlEncoding(string $xml, string $encoding, string $message = ''): void
  {
    if (!$message)
      $message = sprintf("Document:\n\n%s\n", $xml);

    $document = $this->_getDocument($xml);
    if ($document->xmlEncoding != $encoding)
      throw new XPathException($message . sprintf(
        'Failed asserting that XML encoding of document "%s" EQUALS "%s"',
        $document->xmlEncoding,
        $encoding
      ));
  }


  protected function _getNodeList(string $xml, string $path): DOMNodeList
  {
    if (!$nodeList = $this->newXPathForXml($xml)->query($path))
      throw new XPathException(sprintf('Error querying XPath "%s"', $path));

    return $nodeList;
  }


  protected function _assertNodeListNotEmpty(
    DOMNodeList $nodeList,
    string $path,
    string $message
  ): void {
    if ($nodeList->length == 0)
      throw new XPathException($message . sprintf(
        'Failed asserting that node denoted by "%s" EXISTS',
        $path
      ));
  }


  protected function _getDocument($xml): DOMDocument
  {
    $document = new DOMDocument;
    if (!$document->loadXML($xml))
      throw new XPathException('Error parsing document');

    return $document;
  }
}
