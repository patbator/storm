<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Testing;

use Closure;


class MethodRedirection
{
  protected $_value_to_answer;
  protected ?Closure $_closure_to_run = null;
  protected array $_expected_params = [];
  protected string $_method;
  protected ObjectWrapper $_wrapper;
  protected bool $_should_not_be_called = false;

  public static function onWrapper(ObjectWrapper $wrapper): self
  {
    $redirection = new self();
    return $redirection->setWrapper($wrapper);
  }


  public function answers($value_to_answer): ObjectWrapper
  {
    $this->_value_to_answer = $value_to_answer;
    return $this->getWrapper();
  }


  public function willDo(Closure $closure): ObjectWrapper
  {
    $this->_closure_to_run = $closure;
    return $this->getWrapper();
  }


  public function getWrapper(): ObjectWrapper
  {
    return $this->_wrapper;
  }


  public function beStrict(): ObjectWrapper
  {
    return $this->getWrapper()->beStrict();
  }


  public function setWrapper(ObjectWrapper $wrapper): self
  {
    $this->_wrapper = $wrapper;
    return $this;
  }


  public function whenCalled(string $method_name): self
  {
    return $this->getWrapper()->whenCalled($method_name);
  }


  public function shouldNotBeCalled(string $method_name): self
  {
    return $this->getWrapper()->shouldNotBeCalled($method_name);
  }


  public function setMethod(string $method): self
  {
    $this->_method = $method;
    return $this;
  }


  public function _raiseShouldNotBeCalledException(): void
  {
    throw new MethodRedirectionException(
      sprintf(
        "Method %s(%s) was not expected to be called",
        $this->_method,
        implode(',', $this->_expected_params)
      )
    );
  }


  public function never(): self
  {
    $this->_should_not_be_called = true;
    return $this;
  }


  public function getValueToAnswer(array $args)
  {
    if ($this->_should_not_be_called)
      $this->_raiseShouldNotBeCalledException();

    if (null !== $this->_closure_to_run)
      return call_user_func_array($this->_closure_to_run, $args);

    return $this->_value_to_answer;
  }


  public function with(): self
  {
    $this->_expected_params = func_get_args();
    return $this;
  }


  public function matchMethod(string $method): bool
  {
    return $method == $this->_method;
  }


  public function matchMethodAndArgs(string $method, array $args): bool
  {
    return $this->matchMethod($method) and $this->matchArgs($args);
  }


  public function matchArgs(array $args): bool
  {
    return ($args == $this->_expected_params);
  }


  public function expectArgs(): bool
  {
    return count($this->_expected_params) > 0;
  }
}
