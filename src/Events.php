<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2021 Patrick Barroca and contributors, see CONTRIBUTORS.txt


  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm;

use Storm\Events\Event;


class Events
{
    protected static ?Events $_instance = null;

    protected Set $_observers;

    /** @category testing */
    public static function setInstance(?Events $instance) : void
    {
        static::$_instance = $instance;
    }


    public static function getInstance() : self
    {
        return static::$_instance ??= new static();
    }


    public function __construct()
    {
        $this->_observers = new Set;
    }


    /**
     * @param $observer callable
     */
    public function register($observer) : self
    {
        $this->_observers->append($observer);
        return $this;
    }


    /**
     * @param $observer callable
     */
    public function unregister($observer) : self
    {
        $this->_observers = $this->_observers->without($observer);
        return $this;
    }


    public function notify(Event $event) : self
    {
        $this->_observers->eachDo(fn($observer) => $observer($event));
        return $this;
    }
}
