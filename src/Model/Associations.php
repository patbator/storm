<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model;

use Storm\Collection;


/**
 * Hold all Storm_Model_Association betwen Storm models
 */
class Associations
{
    /**
     * @var array of Storm\Model\Association\AbstractAssociation subclass
     */
    protected $_associations;


    public function __construct()
    {
        $this->_associations = new Collection();
    }


    /**
     * Add an association to this model. Ex:
     *
     * $associations
     *       ->add(new Storm_Model_Association_HasOne('brain', ['model' => 'Storm_Test_VolatileBrain',
     *                                                          'referenced_in' => 'brain_id']))
     *       ->add(new Storm_Model_Association_HasOne('mouth', ['model' => 'Storm_Test_VolatileMouth',
     *                                                          'referenced_in' => 'id_mouth']));
     *
     * @param $association Storm\Model\Association\AbstractAssociation subclass
     */
    public function add($association)
    {
        $this->_associations->append($association);
        return $this;
    }


    /**
     * @param $name string
     * @return Storm\Association\AbstractAssociation
     */
    public function at($name)
    {
        return $this->_associations->detect(function($association) use ($name) {
            return $association->isNamed($name);
        });
    }


    /**
     * @return Storm\Collection
     */
    public function hasManyAssociations()
    {
        return $this->_associations->collect(function($association) {
            return $association->isHasMany();
        });
    }


    /**
     * To be called by the model root of the associations.
     *
     * see Storm_Model_Association_Abstract::perform
     */
    public function handleCall($model, $method, $args)
    {
        $match = $this->_associations->detect(function($association) use ($method) {
            return $association->canHandle($method);
        });

        return $match
            ? $match->perform($model, $method, $args)
            : $model->perform($method, $args);
    }


    /**
     * Associations must be saved
     * @return mixed
     */
    public function save($model)
    {
        $this->eachDo(function($association) use ($model) {
            $association->save($model);
        });
    }


    /**
     * Associations must be saved before model
     * @return mixed
     */
    public function saveBefore($model)
    {
        $this->eachDo(function($association) use ($model) {
            $association->saveBefore($model);
        });
    }


    /**
     * Associations must be deleted
     * @return mixed
     */
    public function delete($model)
    {
        $this->eachDo(function($association) use ($model) {
            $association->delete($model);
        });
    }


    /**
     * Do something with each association
     * @param $do callable
     * @return Storm\Model\Associations
     */
    public function eachDo($do)
    {
        $this->_associations->eachDo($do);
        return $this;
    }
}
