<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace Storm\Model\Loader;


class Cache
{
  protected
    $_key_prefix,
    $_cache;


  public function __construct($key_prefix, $cache)
  {
    $this->_key_prefix = $key_prefix;
    $this->_cache = $cache;
  }


  /**
   * @param $classname string name of Storm_Model_Abstract subclass of instance to load
   * @param $id integer $id of the instance to load
   */
  public function load($classname, $id)
  {
    $key = md5(serialize([$this->_key_prefix, $classname, $id]));
    if ($serialized_instance = $this->_cache->load($key))
      return unserialize($serialized_instance);

    return null;
  }


  /**
   * @param $instance Storm_Model_Abstract subclass
   */
  public function save($instance)
  {
    $key = md5(serialize([$this->_key_prefix, get_class($instance), $instance->getId()]));
    return $this->_cache->save(serialize($instance), $key);
  }
}
