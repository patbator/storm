<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Association;


class HasManyOnCallBack extends HasMany
{
    protected $_instances = [];


    public function perform($model, $method, $args)
    {
        return $this->_withInstanceDo(function($instance) use($model, $method, $args) {
            return $instance->perform($model, $method, $args);
        });
    }


    /**
     * Association must be saved
     *
     * @return mixed
     */
    public function save($model)
    {
        return $this->_withInstanceDo(function($instance) use($model) {
            return $instance->save($model);
        });
    }


    /**
     * Association must be saved before model
     *
     * @return mixed
     */
    public function saveBefore($model)
    {
        return $this->_withInstanceDo(function($instance) use($model) {
            return $instance->saveBefore($model);
        });
    }


    /**
     * Association must be deleted
     *
     * @return mixed
     */
    public function delete($model)
    {
        return $this->_withInstanceDo(function($instance) use($model) {
            return $instance->delete($model);
        });
    }


    protected function _withInstanceDo($closure)
    {
        $description = call_user_func($this->getOption('callback'));
        $hash = $this->_hashOf($description);
        if (!array_key_exists($hash, $this->_instances))
            $this->_instances[$hash] = (new Factory())->buildHasManyFor($this->_name, $description);

        return $closure($this->_instances[$hash]);
    }


    protected function _hashOf($description)
    {
        return md5(serialize($description));
    }
}
