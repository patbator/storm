<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Association;

use Storm\Inflector;


class HasMany extends AbstractAssociation
{
    protected
        $_dependents,
        $_dependents_in_db;

    public function isHasMany()
    {
        return true;
    }


    public function canHandle($method)
    {
        if (parent::canHandle($method))
            return true;

        if (!preg_match('/(add|remove)(\w+)/', $method, $matches))
            return false;

        return Inflector::singularize($this->_name) == Inflector::underscorize($matches[2]);
    }


    public function perform($model, $method, $args)
    {
        if ('add' == substr($method, 0, 3))
            return $this->_add($model, $args[0]);

        if ('remove' == substr($method, 0, 6))
            return $this->_remove($model, $args[0]);

        return parent::perform($model, $method, $args);
    }


    public function delete($model)
    {
        if ('delete' != $this->getOption('dependents'))
            return;

        foreach($this->_get($model) as $dependent) {
            $this->_remove($model, $dependent);
            $dependent->delete();
        }
    }


    public function save($model) {
        if (!isset($this->_dependents))
            return $model;

        if (!isset($this->_dependents_in_db))
            $this->_dependents_in_db = $this->_load($model);

        $dependents = (array)$this->_dependents;
        $dependents_in_db = (array)$this->_dependents_in_db;
        $dependents_to_delete = $this->_array_diff(
            $dependents_in_db,
            $dependents
        );

        foreach ($dependents_to_delete as $dependent)
            $dependent->delete();

        foreach ($dependents as $dependent)
            $dependent->save();

        $this->_dependents_in_db = $this->_dependents = $dependents;
        return $model;
    }


    protected function _get($model)
    {
        if (null !== $this->_dependents)
            return $this->_wrap($this->_dependents);

        return $this->_store($this->_load($model));
    }


    protected function _store($dependents)
    {
        if (!isset($this->_dependents_in_db))
            $this->_dependents_in_db = $dependents;

        return $this->_dependents = $dependents;
    }


    protected function _load($model)
    {
        if ($model->isNew())
            return [];

        $find_params = [$this->_idFieldForRole() => $model->getId()];

        if ($this->hasOption('order'))
            $find_params['order'] = $this->getOption('order');

        if ($this->hasOption('limit'))
            $find_params['limit'] = $this->getOption('limit');

        if ($this->hasOption('scope'))
            foreach($this->getOption('scope') as $field => $value)
                $find_params[$field] = $value;

        $dependents = call_user_func([$this->getOption('model'), 'findAllBy'], $find_params);

        return $this->_wrap(array_filter($dependents));
    }


    protected function _has($model)
    {
        return !empty($this->_get($model));
    }


    protected function _numberOf($model)
    {
        if (isset($this->_dependents))
            return count($this->_dependents);

        if ($model->isNew())
            return 0;

        $params = [$this->_idFieldForRole() => $model->getId()];
        if ($scope = $this->getOption('scope'))
            $params['scope'] = $scope;

        return call_user_func([$this->getOption('model'), 'countBy'], $params);
    }


    protected function _idFieldForRole() {
        $class_name = $this->getOption('model');
        $instance = new $class_name;
        return $instance->getIdFieldForDependent($this->getOption('role'));
    }


    protected function _set($model, $value)
    {
        $dependents = (array)$this->_load($model);
        $dependents_to_remove = $this->_array_diff(
            $dependents,
            $value
        );

        foreach($dependents_to_remove as $dependent)
            $this->_remove($model, $dependent);

        foreach($value as $dependent)
            $this->_add($model, $dependent);

        return $model;
    }


    protected function _add($model, $value)
    {
        if (!$value)
            return $model;

        $dependents = $this->_get($model);

        if ($this->_isUniqueConstraintViolated($dependents, $value))
            return $model;

        $dependents[] = $value;
        $this->_dependents = $dependents;
        $value->callSetterByAttributeName($this->getOption('role'), $model);

        return $model;
    }


    protected function _remove($model, $value)
    {
        if (!$value)
            return $model;

        $dependents = $this->_get($model);
        $dependents_without_value = [];
        foreach ($dependents as $dependent)
            if ((!$value->isNew() && ($dependent->getId() != $value->getId()))
                || ($value !== $dependent))
                $dependents_without_value[] = $dependent;

        $this->_dependents = $dependents_without_value;

        return $model;
    }


    protected function _isUniqueConstraintViolated($dependents, $value)
    {
        if (true !== $this->getOption('unique'))
            return false;

        if ($value->isNew())
            return false;

        foreach($dependents as $dependent)
            if ($dependent->getId() == $value->getId())
                return true;

        return false;
    }


    protected function _wrap($dependents)
    {
        if (!$class = $this->getOption('instance_of'))
            return $dependents;

        return is_a($dependents, $class)
            ? $dependents
            : new $class($dependents);
    }


    protected function _array_diff($array1, $array2)
    {
        $diff = [];
        foreach($array1 as $element)
            if (!in_array($element, $array2))
                $diff[] = $element;

        return $diff;
    }
}
