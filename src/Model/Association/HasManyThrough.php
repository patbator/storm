<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Association;

use Storm\Inflector;
use Storm\Model\Collection;


class HasManyThrough extends HasMany
{
    public function save($model)
    {
        return $model;
    }


    protected function _get($model)
    {
        return $this->_load($model);
    }


    protected function _load($model)
    {
        return $this->_wrap($this->_loadThrough($model));
    }


    protected function _loadThrough($model)
    {
        $through = $model->callGetterByAttributeName($this->getOption('through'));
        $association = $model->associationAt($this->getOption('through'));
        if ($association->isBelongsTo())
            $through = [$through];

        return $this->_getForInstances($through);
    }


    protected function _getForInstances($instances)
    {
        $singularized_field = Inflector::singularize($this->_name);
        $getManyMethod = 'get' . ucfirst($this->_name);
        $getOneMethod = 'get' . ucfirst($singularized_field);

        $dependents = [];
        foreach ($instances as $instance)
            $instance->hasBelongsToRelashionshipWith($singularized_field)
                ? $dependents[] = $instance->$getOneMethod()
                : $dependents = array_merge($dependents, $instance->$getManyMethod());

        return array_filter($dependents);
    }


    protected function _add($model, $value)
    {
        if (!$value)
            return $model;

        $dependents = $this->_get($model);

        if ($this->_isUniqueConstraintViolated($dependents, $value))
            return $model;

        $association = $model->associationAt($this->getOption('through'));
        if ($association->isBelongsTo()
            && ($existing = $model->callGetterByAttributeName($this->getOption('through')))) {
            $existing->callAdderByAttributeName($this->_name, $value);
            return $model;
        }

        $role = $association->getOption('role');
        $model_name = $association->getOption('model');

        $intermediate = new $model_name;
        $intermediate->callSetterByAttributeName($role, $model);

        $singularized_field = Inflector::singularize($this->_name);
        $intermediate->hasBelongsToRelashionshipWith($singularized_field)
            ? $intermediate->callSetterByAttributeName($singularized_field, $value)
            : $intermediate->callAdderByAttributeName($this->_name, $value);

        $model->callAdderByAttributeName($this->getOption('through'), $intermediate);

        return $model;
    }


    protected function _remove($model, $value)
    {
        if (!$value)
            return $model;

        return $this->_removeThrough($model, $value);
    }


    protected function _removeThrough($model, $value)
    {
        if ($dependent = $this->_findDependentOfModelAndValueThrough($model, $value)) {
            $method = 'remove'.Inflector::camelize(Inflector::singularize($this->getOption('through')));
            $model->$method($dependent);
        }

        return $model;
    }


    protected function _findDependentOfModelAndValueThrough($model, $value)
    {
        if (!$throughs = $model->callGetterByAttributeName($this->getOption('through')))
            return;

        $association = null;
        $my_attribute = Inflector::singularize($this->_name);

        foreach($throughs as $through) {
            if (!$association)
                $association = $through->associationAt($my_attribute);

            if (!$association)
                return;

            if (null == ($other_value = $through->callGetterByAttributeName($my_attribute)))
                continue;

            if ($value->getId() == $other_value->getId())
                return $through;
        }
    }


    protected function _numberOf($model)
    {
        if ($this->getOption('unique')) {
            $method = 'numberOf' . ucfirst(Inflector::camelize($this->getOption('through')));
            return $model->$method();
        }

        if (!$throughs = $model->callGetterByAttributeName($this->getOption('through')))
            return 0;

        $association = $model->associationAt($this->getOption('through'));
        if ($association->isBelongsTo())
            $throughs = [ $throughs ];

        return $this->_numberForInstances($throughs);
    }


    protected function _numberForInstances($instances)
    {
        if (!$instances)
            return 0;

        $method = $this->_methodWithPrefix('numberOf', $instances[0]);

        $number_of = 0;
        foreach ($instances as $instance)
            $number_of += $instance->$method();

        return $number_of;
    }


    protected function _methodWithPrefix($prefix, $dependent)
    {
        $singularized_field = Inflector::singularize($this->_name);
        $manyMethod = $prefix . ucfirst(Inflector::camelize($this->_name));
        $oneMethod = $prefix . ucfirst(Inflector::camelize($singularized_field));

        return $dependent->hasBelongsToRelashionshipWith($singularized_field)
            ? $oneMethod
            : $manyMethod;
    }
}
