<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Association;


class BelongsTo extends AbstractAssociation
{
    protected $_dependent;

    public function isBelongsTo()
    {
        return true;
    }


    public function getReferencedIn()
    {
        return $this->getOption('referenced_in', $this->_name . '_id');
    }


    protected function _get($model)
    {
        if (isset($this->_dependent))
            return $this->_dependent;

        $dependent_id = $model->callGetterByAttributeName($this->getReferencedIn());
        return $this->_dependent = call_user_func_array([$this->_options['model'], 'find'],
                                                        [$dependent_id]);
    }


    protected function _has($model)
    {
        return !empty($this->_get($model));
    }


    protected function _numberOf($model)
    {
        return $this->_has($model) ? 1 : 0;
    }


    protected function _set($model, $value)
    {
        $this->_dependent = $value;
        return $this->_updateDependentId($model);
    }


    protected function _updateDependentId($model)
    {
        return $model
            ->callSetterByAttributeName($this->getReferencedIn(),
                                        $this->_dependent ? $this->_dependent->getId() : null);
    }


    public function saveBefore($model)
    {
        if (!$this->_dependent)
            return;

        if ($this->_dependent->isNew())
            $this->_dependent->save();

        $this->_updateDependentId($model);
    }
}
