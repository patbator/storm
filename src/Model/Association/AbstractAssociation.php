<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Association;

use Storm\Inflector;


/**
 * Base class for associations between Storm models.
 * To register an association, a model must define the function describeAssocitionsOn. Ex:
 *
 *	public function describeAssociationsOn($associations) {
 *    $associations
 *          ->add(new Storm_Model_Association_HasOne('brain', ['model' => 'Storm_Test_VolatileBrain',
 *                                                             'referenced_in' => 'brain_id']))
 *          ->add(new Storm_Model_Association_HasOne('mouth', ['model' => 'Storm_Test_VolatileMouth',
 *                                                             'referenced_in' => 'id_mouth']));
 *  }
 *
 * @see Storm\Model\Associations.
 */
abstract class AbstractAssociation
{
    protected
        /**
         * @var string the name of association. May be used
         * to determine the selectors added to the model
         * that will trigger this association.
         * Ex:
         * new Storm_Model_Association_HasOne('brain', ...)
         * will add getBrain and setBrain.
         */
        $_name,

        /** @var array of key/value options, see subclasses */
        $_options;


    public function __construct($name, $options = [])
    {
        $this->_name = $name;
        $this->_options = $options;
    }


    public function getName()
    {
        return $this->_name;
    }


    public function isNamed($name)
    {
        return $this->_name == $name;
    }


    public function isBelongsTo()
    {
        return false;
    }


    public function isHasMany()
    {
        return false;
    }


    public function getOption($name, $default=null)
    {
        return $this->hasOption($name)
            ? $this->_options[$name]
            : $default;
    }


    public function hasOption($name)
    {
        return array_key_exists($name, $this->_options);
    }


    /**
     * @param $method string
     * @return boolean true if this association can handle given method name
     */
    public function canHandle($method)
    {
        if (!preg_match('/(get|set|has|numberOf)(\w+)/', $method, $matches))
            return false;

        return $this->_name == Inflector::underscorize($matches[2]);
    }


    /**
     * Perform the action handled by this association (usually getXXX or setXXX)
     *
     * @param $model Storm\Model\ModelAbstract
     * @param $method string
     * @param $args array
     * @return mixed
     */
    public function perform($model, $method, $args)
    {
        if ('get' == substr($method, 0, 3))
            return $this->_get($model);

        if ('has' == substr($method, 0, 3))
            return $this->_has($model);

        if ('numberOf' == substr($method, 0, 8))
            return $this->_numberOf($model);

        if ('set' == substr($method, 0, 3))
            $this->_set($model, $args[0]);

        return $model;
    }


    abstract protected function _get($model);

    abstract protected function _has($model);

    abstract protected function _numberOf($model);

    abstract protected function _set($model, $value);


    /**
     * Association must be saved
     *
     * @return mixed
     */
    public function save($model) {}


    /**
     * Association must be saved before model
     *
     * @return mixed
     */
    public function saveBefore($model) {}


    /**
     * Association must be deleted
     *
     * @return mixed
     */
    public function delete($model) {}
}
