<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace Storm\Model;


class HierarchyVisitor
{
  protected $_hierarchy = [];

  public function visit($model, $from, $link_name)
  {
    if (!is_object($model))
      return;

    if (!array_key_exists($from, $this->_hierarchy))
      $this->_hierarchy[$from] = [];

    $name = get_class($model);
    $this->_hierarchy[$from][] = new HierarchyVisitorLink($name, $link_name);

    if (!array_key_exists($name, $this->_hierarchy))
      $model->acceptHierarchyVisitor($this);
  }


  public function asArray()
  {
    return $this->_hierarchy;
  }


  public function asDot()
  {
    $dot = '';
    foreach ($this->_hierarchy as $from => $tos) {
      foreach ($tos as $to)
        $dot .= $this->_dot($from, $to);
    }
    return 'digraph G {' . $dot .'}';
  }


  protected function _dot($from, $to)
  {
    return $from . ' -> ' . $to->asDot() . ';';
  }
}


class HierarchyVisitorLink
{
  protected $_target;
  protected $_name;

  public function __construct($target, $name)
  {
    $this->_target = $target;
    $this->_name = $name;
  }


  public function asDot()
  {
    return $this->_target . ' [label="' . $this->_name .'"]';
  }
}
