<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace Storm\Model\Collection;

use Storm\Collection as StormCollection;


/** Base class for all has_many relationships > instance_of property*/
abstract class CollectionAbstract extends StormCollection
{
  public function collect($closure_or_string) : self
  {
    return
      is_string($closure_or_string)
      ? $this->collect(fn($e) => $e->callGetterByAttributeName($closure_or_string))
      : parent::collect($closure_or_string);
  }


  public function select($closure_or_string) : self
  {
    return
      is_string($closure_or_string)
      ? $this->select(fn($e) => call_user_func([$e, $closure_or_string]))
      : parent::select($closure_or_string);
  }


  public function reject($closure_or_string) : self
  {
    return
      is_string($closure_or_string)
      ? $this->select(fn($e) => !call_user_func([$e, $closure_or_string]))
      : parent::reject($closure_or_string);
  }


  public function eachDo($closure_or_string) : self
  {
    return is_string($closure_or_string)
      ? $this->eachDo(fn($model) => call_user_func([$model, $closure_or_string]))
      : parent::eachDo($closure_or_string);
  }
}
