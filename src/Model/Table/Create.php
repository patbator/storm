<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2018- Patrick Barroca and contributors, see CONTRIBUTORS.txt
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Table;

use Storm\Model\Table\CommandPart\Table;
use Storm\Model\Table\CommandPart;


class Create
{
    protected
        $_table,
        $_fields,
        $_engine;


    public function __construct()
    {
        $this->_table = new Table($this);
        $this->_fields = [];
        $this->_engine = new Engine($this);
    }


    public function table($name)
    {
        $this->_table->setName($name);
        return $this;
    }


    public function engine($name)
    {
        $this->_engine->setName($name);
        return $this;
    }


    public function defaultPrimary()
    {
        $this->int('id')->notNull()->incrementable()->primary();
        return $this;
    }


    public function __call($name, $args)
    {
        $map = [
            'int' => Integer::class,
            'char' => Character::class,
            'date' => Date::class,
            'blob' => Blob::class,
            'text' => Text::class,
        ];

        if (array_key_exists($name, $map)) {
            $class = $map[$name];
            return $this->_fields[] = (new $class($this))->named($args[0]);
        }

        throw new \RuntimeException('Call to unknown method ' . $name);
    }


    public function acceptVisitor($visitor)
    {
        $this->_table->acceptVisitor($visitor);
        foreach($this->_fields as $part)
            $part->acceptVisitor($visitor);
        $this->_engine->acceptVisitor($visitor);
    }
}



class Engine extends CommandPart
{
    protected $_name = 'MyISAM';

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }


    public function acceptVisitor($visitor)
    {
        $visitor->visitEngine($this->_name);
    }
}



class Field extends CommandPart
{
    const
        KEY_PRIMARY = 'primary',
        KEY_UNIQUE = 'unique',
        KEY_MULTIPLE = '';

    protected
        $_name,
        $_nullable = true,
        $_default,
        $_index;

    public function named($name)
    {
        $this->_name = $name;
        return $this;
    }


    public function primary()
    {
        $this->_index = static::KEY_PRIMARY;
        return $this;
    }


    public function indexed()
    {
        $this->_index = static::KEY_MULTIPLE;
        return $this;
    }


    public function unique()
    {
        $this->_index = static::KEY_UNIQUE;
        return $this;
    }


    public function notNull()
    {
        $this->_nullable = false;
        return $this;
    }


    public function acceptVisitor($visitor)
    {
        $parts = explode('\\', get_class($this));
        $method = 'visit' . ucfirst(end($parts));
        call_user_func_array([$visitor, $method], $this->_visitorParams());
    }


    protected function _visitorParams()
    {
        return [$this->_name, $this->_nullable, $this->_index, $this->_default];
    }
}



class Integer extends Field
{
    protected
        $_incrementable = false,
        $_sign;

    public function incrementable()
    {
        $this->_incrementable = true;
        return $this;
    }


    protected function _visitorParams()
    {
        $params = parent::_visitorParams();
        $params[] = $this->_incrementable;
        return $params;
    }
}


class Character extends Field
{}


class Date extends Field
{}


class Blob extends Field
{}


class Text extends Field
{}
