<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Table;

use Storm\Model\Table\CommandPart;
use Storm\Model\Table\CommandPart\Table;
use Storm\Model\Table\CommandPart\Where;
use Storm\Model\Table\CommandPart\Order;
use Storm\Model\Table\CommandPart\Limit;


class Select
{
    protected
        $_fields,
        $_from,
        $_where,
        $_order,
        $_limit;


    public function __construct()
    {
        $this->_fields = new SelectAllFields($this);
        $this->_from = new Table($this);
        $this->_where = new Where($this);
        $this->_order = new Order($this);
        $this->_limit = new Limit($this);
    }


    public function withAllFields()
    {
        $this->_fields = new SelectAllFields($this);
        return $this;
    }


    public function withCount($name)
    {
        $this->_fields = (new SelectCountField($this))->on($name);
        return $this;
    }


    public function fields($fields)
    {
        $this->_fields->setFields($fields);
        return $this;
    }


    public function from($table)
    {
        $this->_from->setName($table);
        return $this;
    }


    public function where()
    {
        return $this->_where;
    }


    public function limit()
    {
        return $this->_limit;
    }


    public function order()
    {
        return $this->_order;
    }


    public function acceptVisitor($visitor)
    {
        foreach([$this->_fields, $this->_from, $this->_where,
                 $this->_order, $this->_limit]
                as $part)
            $part->acceptVisitor($visitor);
    }
}



class SelectAllFields extends CommandPart
{
    public function acceptVisitor($visitor)
    {
        $visitor->visitFields('*');
    }
}


class SelectCountField extends CommandPart
{
    protected $_name;


    public function on($name)
    {
        $this->_name = $name;
        return $this;
    }


    public function acceptVisitor($visitor)
    {
        $visitor->visitFields('count(' . $this->_name . ') as numberOf');
    }
}
