<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model\Table\CommandPart;

use Storm\Model\Table\CommandPart;

class Where extends CommandPart
{
    protected $_predicates = [];

    public function __call($name, $params)
    {
        $field_and_value = [
            'equalsTo' => EqualsTo::class,
            'notEqualsTo' => NotEqualsTo::class,
            'isIn'     => IsIn::class,
            'isNotIn'  => IsNotIn::class,
        ];

        if (array_key_exists($name, $field_and_value)) {
            $class = $field_and_value[$name];
            $this->_predicates[] = new $class($params[0], $params[1]);
            return $this;
        }

        $field_or_value = [
            'isNull'    => IsNull::class,
            'isNotNull' => IsNotNull::class,
            'literal'   => LiteralClause::class,
        ];

        if (array_key_exists($name, $field_or_value)) {
            $class = $field_or_value[$name];
            $this->_predicates[] = new $class($params[0]);
            return $this;
        }

        return parent::__call($name, $params);
    }


    public function acceptVisitor($visitor)
    {
        foreach($this->_predicates as $predicate)
            $predicate->acceptVisitor($visitor);
    }
}



class Predicate
{
    protected $_visitor_method;

    protected function _visitorParams()
    {
        return [];
    }


    public function acceptVisitor($visitor)
    {
        call_user_func_array(
            [$visitor, $this->_visitor_method],
            $this->_visitorParams()
        );
    }
}



class PredicateFieldAndValue extends Predicate
{
    protected
        $_field,
        $_value;

    public function __construct($field, $value)
    {
        $this->_field = $field;
        $this->_value = $value;
    }


    protected function _visitorParams()
    {
        return [$this->_field, $this->_value];
    }
}



class PredicateFieldOrValue extends Predicate
{
    protected
        $_value;

    public function __construct($value)
    {
        $this->_value = $value;
    }


    protected function _visitorParams()
    {
        return [$this->_value];
    }
}



class EqualsTo extends PredicateFieldAndValue
{
    protected $_visitor_method = 'visitFieldEqualsTo';
}



class NotEqualsTo extends PredicateFieldAndValue
{
    protected $_visitor_method = 'visitFieldNotEqualsTo';
}



class LiteralClause extends PredicateFieldOrValue
{
    protected $_visitor_method = 'visitLiteralClause';
}



class IsNull extends PredicateFieldOrValue
{
    protected $_visitor_method = 'visitFieldIsNull';
}



class IsNotNull extends PredicateFieldOrValue
{
    protected $_visitor_method = 'visitFieldIsNotNull';
}



class IsIn extends PredicateFieldAndValue
{
    protected $_visitor_method = 'visitFieldIsIn';
}



class IsNotIn extends PredicateFieldAndValue
{
    protected $_visitor_method = 'visitFieldIsNotIn';
}
