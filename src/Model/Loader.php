<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model;

use Storm\Persistence\VolatileStrategy;
use Storm\Model\Loader\NullCache;

/**
 * Load data from database for a given model
 *
 * Model has to
 *  - extend Storm_Model_Abstract
 *  - define its table name
 *  - implement static getLoader (will be deprecated when PHP 5.3 compatibility arise)
 *
 * @example
 * class MyModel extends Storm_Model_Abstract {
 *  public $_table_name='my_model';
 *
 *  public static function getLoader() {
 *    return self::getLoaderFor(__CLASS__);
 *  }
 * }
 */
class Loader
{
    /**
     * @var Storm_Model_Loader_Cache
     */
    protected static $_loader_cache;

    protected static $_default_strategy = VolatileStrategy::class;

    /**
     * @param $cache Zend_Cache
     */
    public static function setDefaultCache($cache)
    {
        static::$_loader_cache = $cache;
    }


    public static function setDefaultStrategy($strategy)
    {
        static::$_default_strategy = $strategy;
    }


    public static function defaultToVolatile()
    {
        static::setDefaultStrategy(VolatileStrategy::class);
    }


    public static function defaultTo($class_name)
    {
        static::setDefaultStrategy($class_name);
    }


    public static function resetCache()
    {
        static::$_loader_cache = null;
    }


    /**
     * @var string
     */
    protected $_model;

    /**
     * @var Storm_Model_Table
     */
    protected $_table;

    /**
     * @var string
     */
    protected $_id_field;

    /**
     * @var string
     */
    protected $_table_name;

    /**
     * @var array
     */
    protected $_loaded_instances = [];

    /**
     * @var Storm\Persistence\AbstractStrategy
     */
    protected $_persistence_strategy;


    /**
     * @param string $class
     */
    public function __construct($class)
    {
        $this->_model = $class;
        $this->_table_name = ($table = $class::getClassVar('_table_name'))
            ? strtolower($table)
            : '';

        $this->_id_field = ($primary = $class::getClassVar('_table_primary'))
            ? strtolower($primary)
            : 'id';
    }


    /**
     * @param Storm_Model_Table $tbl
     * @return Storm_Model_Loader
     */
    public function setTable($tbl)
    {
        $this->getPersistenceStrategy()->setTable($tbl);
        return $this;
    }


    /**
     * @return Storm_Model_Table
     */
    public function getTable()
    {
        return $this->getPersistenceStrategy()->getTable();
    }


    public function getModel()
    {
        return $this->_model;
    }


    /**
     * @return string
     */
    public function getIdField()
    {
        return $this->_id_field;
    }


    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }


    /**
     * @param array $param
     * @return array
     */
    public function findAll($params=[])
    {
        return $this->getPersistenceStrategy()->findAll($params);
    }


    public function getLoaderCache()
    {
        if (!static::$_loader_cache)
            static::$_loader_cache = new NullCache();

        return static::$_loader_cache;
    }


    public function getPersistenceStrategy()
    {
        if (isset($this->_persistence_strategy))
            return $this->_persistence_strategy;

        $class_name = static::$_default_strategy;
        return $this->_persistence_strategy = new $class_name($this);
    }


    /** @category testing */
    public function setPersistenceStrategy($strategy)
    {
        $this->_persistence_strategy = $strategy;
        return $this;
    }


    public function beVolatile()
    {
        if (!$this->getPersistenceStrategy()->isVolatile())
            $this->_persistence_strategy = new VolatileStrategy($this);
        return $this;
    }


    public function isVolatile()
    {
        return $this->getPersistenceStrategy()->isVolatile();
    }

    /**
     * @param int $id
     * @return Storm_Model_Abstract
     */
    public function find($id)
    {
        if (null===$id)
            return null;

        if (isset($this->_loaded_instances[$id]))
            return $this->_loaded_instances[$id];

        if ($instance = $this->getLoaderCache()->load($this->_model, $id))
            return $instance;

        if ($instance = $this->getPersistenceStrategy()->find($id)) {
            $this->cacheInstance($instance);
            return $instance;
        }

        return null;
    }


    /**
     * Add a model into runtime cache
     *
     * @param Storm_Model_Abstract $instance
     * @return Storm_Model_Loader
     */
    public function cacheInstance($instance)
    {
        $this->_loaded_instances[$instance->getId()] = $instance;
        $this->getLoaderCache()->save($instance);

        return $this;
    }


    public function getLoadedInstances()
    {
        return $this->_loaded_instances;
    }


    /**
     * Clear instance cache
     * @return Storm_Model_Loader
     */
    public function clearCache()
    {
        $this->_loaded_instances = [];
        return $this;
    }


    /**
     * @param array $attributes default attributes values
     * @return a new instance of my model
     */
    public function newInstance($attributes = null)
    {
        $class = $this->_model;
        $instance = new $class();

        if (!empty($attributes))
            $instance->updateAttributes($attributes);

        return $instance;
    }


    /**
     * Create a new instance and cache it
     * @param mixed $id primary key
     * @param array $attributes default attributes values
     * @return Storm_Model_Abstract
     */
    public function newInstanceWithId($id, $attributes = null)
    {
        $instance = $this
            ->newInstance()
            ->setId($id); //id must be set BEFORE other attributes (they may depend on the id's value)

        if (!empty($attributes))
            $instance->updateAttributes($attributes);

        $this->cacheInstance($instance);

        return $instance;
    }


    public function newInstanceWithIdAssertSave($id, $attributes = null)
    {
        $instance = $this->newInstanceWithId($id,$attributes);
        $instance->assertSave();

        return $instance;
    }


    /**
     * Create an instance and assigns its
     * attributes using an associative array
     * (name_of_attribute => value)
     *
     * @param array $row
     * @return Storm_Model_Abstract
     */
    public function newFromRow($row)
    {
        $row = array_change_key_case($row, CASE_LOWER);
        $id = $row[$this->getIdField()];
        unset($row[$this->getIdField()]);

        if (array_key_exists($id, $this->_loaded_instances))
            return $this->_loaded_instances[$id];

        return $this
            ->newInstanceWithId($id)
            ->initializeAttributes($row);
    }


    /**
     * Insert (if new) or update the record in DB
     *
     * @param Storm_Model_Abstract $model
     * @return int
     */
    public function save($model)
    {
        $data = array_filter($model->attributesToArray(),
                             [$this, 'isValueCanBeSaved']);

        $id = isset($data['id']) ? $data['id'] : null;
        unset($data['id']);

        if ($model->isNew()) {
            if ($result = $this->getPersistenceStrategy()->insert($data)) {
                $insert_id = ($model->isFixedId()) ?
                    $data[$this->_id_field] :
                    $this->getPersistenceStrategy()->lastInsertId();

                $model->setId($insert_id);
            }

            return $result;
        }

        $data[$this->_id_field] = $id;

        return $this->getPersistenceStrategy()
            ->update($data, $id);
    }


    /**
     * @return false if value is not saveable in db
     */
    public function isValueCanBeSaved($value)
    {
        return is_string($value)
            || is_int($value)
            || is_float($value)
            || is_bool($value)
            || (null === $value);
    }


    /**
     * @param Storm_Model_Abstract $model
     * @return boolean wheter something has been deleted
     */
    public function delete($model)
    {
        $deleted_count = $this->getPersistenceStrategy()
            ->delete($model);

        unset($this->_loaded_instances[$model->getId()]);

        return 0 < $deleted_count;
    }


    /**
     * @param Array
     */
    public function deleteBy($clauses, $page_size=100)
    {
        $done = 0;
        do {
            $clauses['limit'] = $page_size;
            $models = $this->getPersistenceStrategy()->findAllBy($clauses);
            foreach($models as $model)
                $model->delete();
            $done += count($models);
        } while(count($models) > 0);

        return $done;
    }


    /**
     * @param string $field
     * @return string
     */
    public function getIdFieldForDependent($field)
    {
        $model_instance = new $this->_model;
        return $model_instance->getIdFieldForDependent($field);
    }


    /**
     * @param array $args
     * @return array
     *
     * @example
     * NewsletterSubscription::getLoader()->findAllBy([
     *    'role' => 'newsletter',
     *    'model' => $show
     * ]);
     * Return related rows
     *
     * @example
     * Book::getLoader()->findAllBy(array(
     *    'order' => 'creation_date desc',
     *    'limit' => 10,
     *    'where' => 'note>2'
     *    'tag' => null,
     *    'limitPage' => array($page_number, $items_by_page)
     * ));
     * Return first 10 rows ordered by creation_date where note > 2 and tag is null
     *
     * @example
     * BlueRay::getLoader()->findAllBy(array(
     *    'alpha_key' => 'MILLENIUM',
     *    'user_id' => '12'
     * ));
     * Return rows where alpha_key = 'MILLENIUM' and user_id = '12'
     *
     */
    public function findAllBy($args)
    {
        $filter_given_func = isset($args['callback']) ? $args['callback'] : null;

        $filter_func = null;
        if (isset($args['select'])) {
            $select = $args['select'];
            $filter_func = function($model) use($select) { return $model->$select(); };
        }

        unset($args['select']);
        unset($args['callback']);

        $filter = $filter_func ? $filter_func : $filter_given_func;

        $result = $this->getPersistenceStrategy()->findAllBy($args);
        return  isset($filter) ? array_filter($result, $filter) : $result;
    }


    /**
     * Sends a count request and returns the value
     * @see findAllBy
     * @param array $args
     * @return int
     */
    public function countBy($args)
    {
        return $this->getPersistenceStrategy()->countBy($args);
    }


    /**
     * return number of this model in db
     * @see countBy
     * @return int
     */
    public function count()
    {
        return $this->countBy([]);
    }


    /**
     * @param array $args
     * @return Storm_Model_Abstract
     */
    public function findFirstBy($args)
    {
        $args['limit'] = 1;
        $instances = $this->findAllBy($args);

        if (count($instances) == 0)
            return null;

        $this->cacheInstance($instances[0]);
        return $instances[0];
    }


    public function create()
    {
        return $this->getPersistenceStrategy()
                    ->create($this->createCallback());
    }


    public function createCallback()
    {
        return function($builder) { return $builder; };
    }


    public function drop()
    {
        return $this->getPersistenceStrategy()->drop();
    }
}
