<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Model;

use Storm\Inflector;
use Storm\Model\Association\Factory;
use Storm\Events;
use Storm\Events\Save;
use Storm\Events\Delete;


abstract class ModelAbstract
{
    /**
     * @var array
     */
    protected static $_loaders = [];

    /**
     * @var string
     */
    protected $_table_name = null;

    /**
     * @var string defaults to id
     */
    protected $_table_primary = null;

    /**
     * @var string defaults to Storm\Model\Loader
     */
    protected $_loader_class = null;

    /**
     * @var array
     */
    protected $_attributes = ['id' => null];

    /**
     * @var array
     */
    protected $_attributes_in_db = [];

    /**
     * @var array
     */
    protected $_has_many_attributes_in_db = [];

    /**
     * @var array
     */
    protected $_has_many_attributes = [];

    /**
     * Defines me -> * dependents relationship
     * Attributes:
     *   model:  dependent class name
     *   role: the name of the accessor (also used for default id mapping in db)
     *   order: order statetement while loading models
     *   through: this relation relies on another one
     *   unique: if true, a dependent could not appear twice
     *   dependents: if 'delete', then deleting me will delete dependents
     *   scope: an array of field / value pair that filters data
     *   limit: when specified, add a limit for fetching descendents
     *
     * Ex:
     *  protected $_has_many = array('sessions' => array(
     *                                                 'model' => 'Class_SessionFormation',
     *                                                 'role' => 'formation',
     *                                                 'dependents' => 'delete',
     *                                                 'order' => 'date_debut desc',
     *                                                 'scope' => array('type' => 2)),
     *
     *                               'session_formation_inscriptions' => array('through' => 'sessions',
     *                                                                         'unique' => 'true'),
     *
     *                               'stagiaires' => array('through' => 'session_formation_inscriptions'));
     *
     * Relationships definition can be overrided at runtime by writing a method named descriptionOfFieldName.
     * This can be used to have dynamic relationships between objects.
     *
     * ex:
     *
     * protected $_has_many = [ 'animals' => [] ];
     *
     *  public function descriptionOfAnimals() {
     *      return $this->iWantCats()
     *                  ? [ 'model' => 'Cat', 'role' => 'owner' ]
     *                  : [ 'model' => 'Dog', 'role' => 'owner' ];
     *  }
     *
     *
     * @var array
     */
    protected $_has_many = [];

    /**
     * @var array
     */
    protected $_belongs_to_attributes = [];

    /**
     * Defines me -> * dependents relationship
     * Attributes:
     *   model:  dependent class name
     *   referenced_in: field name used to reference dependent id
     *   trough: this relation relies on another one
     * protected $_belongs_to = array('librairy' => array('model' => 'Librairy',
     *                                                    'referenced_in' => 'id_library'),
     *
     *                                'zone' => array('through' => 'librairy'));
     * @var array
     */
    protected $_belongs_to = [];

    /**
     * Store the list of errors found by validate() method.
     * See also check($condition, $error)
     * @var array
     */
    protected $_errors = [];


    /**
     * Default values for attributes of a new instance
     * Should be defined in subclasses like:
     * $_default_values = array('title' => 'new article', 'content' => '')
     * @var array
     */
    protected $_default_attribute_values = [];


    /**
     * By default, use autoincrement ids generated by the SGBD. When $_fixed_id = true,
     * does not take id from SGBD.
     * @var boolean
     */
    protected $_fixed_id = false;


    /**
     * Generic mean to handle associations (in order to extent and replace has_many and belongs_to)
     * Collection of Storm_Model_Association_Abstract subclasses
     * @var Storm_Collection
     */
    protected $_associations;


    /**
     * @param string $class
     * @return Storm_Model_Loader
     */
    protected static function _buildLoaderFor($class)
    {
        $reflection = new \ReflectionClass($class);
        $class_vars = $reflection->getdefaultProperties();

        if (isset($class_vars['_loader_class'])) {
            $loader_class = $class_vars['_loader_class'];
            return new $loader_class($class);
        }

        return new Loader($class);
    }


    public static function getClassVar($var)
    {
        $reflection = new \ReflectionClass(get_called_class());
        $class_vars = $reflection->getdefaultProperties();

        return $class_vars[$var];
    }


    /**
     * @return Storm_Model_Loader
     */
    public static function getLoader()
    {
        return self::getLoaderFor(get_called_class());
    }


    /**
     * Forward static calls to the Loader so instead of writing:
     *    My_Class::getLoader()->foo()
     * we can write:
     *    My_Class::()
     * @param string $name method name
     * @param array $args
     * @return mixed
     */
    public static function __callStatic($name, $args)
    {
        return call_user_func_array([static::getLoader(), $name], $args);
    }


    /**
     * @param string $class
     * @return Storm_Model_Loader
     */
    public static function getLoaderFor($class)
    {
        return isset(self::$_loaders[$class])
            ? self::$_loaders[$class]
            : self::setLoaderFor($class, self::_buildLoaderFor($class));
    }


    /**
     * @param string $class
     * @param Storm_Model_Loader $loader
     */
    public static function setLoaderFor($class, $loader)
    {
        return self::$_loaders[$class] = $loader;
    }


    public static function unsetLoaders()
    {
        self::$_loaders = [];
    }


    public static function getLoaders()
    {
        return self::$_loaders;
    }


    public function __construct()
    {
        $this->_associations = new Associations();
        $this->buildAssociationsFromConfig();
        $this->describeAssociationsOn($this->_associations);
    }


    public function buildAssociationsFromConfig()
    {
        $factory = new Factory();
        foreach(array_keys($this->_has_many) as $field)
            $this->_associations->add(
                $factory->buildHasManyFor($field, $this->descriptionOf($field))
            );

        foreach(array_keys($this->_belongs_to) as $field)
            $this->_associations->add(
                $factory->buildBelongsToFor($field, $this->descriptionOf($field))
            );
    }


    public function describeAssociationsOn($associations)
    {
    }


    public function associationAt($name)
    {
        return $this->_associations->at($name);
    }


    /**
     * @return bool
     */
    public function hasBelongsToRelashionshipWith($field)
    {
        return ($match = $this->associationAt($field))
            ? $match->isBelongsTo()
            : false;
    }


    /**
     * Put the instance in its Loader's cache
     * @return Storm_Model_Abstract
     */

    public function cache()
    {
        static::getLoader()->cacheInstance($this);
        return $this;
    }


    /**
     * @return bool
     */
    public function save()
    {
        if ($valid = $this->isValid()) {
            $this->saveWithoutValidation();
            $this->_attributes_in_db=$this->_attributes;
        }

        return $valid;
    }


    public function assertSave()
    {
        if (!$this->save())
            throw new Exception('Can\'t save '.get_class($this).': '.implode(',',$this->getErrors()));

        return true;
    }


    public function getClassName()
    {
        return get_class($this);
    }


    public function saveWithoutValidation()
    {
        $this->beforeSave();
        $this->_associations->saveBefore($this);
        Events::getInstance()->notify(new Save($this));
        $this->getLoader()->save($this);
        $this->_associations->save($this);
        $this->afterSave();
    }


    public function beforeSave()
    {}


    public function afterSave()
    {}


    public function beforeDelete()
    {}


    public function afterDelete()
    {}


    /**
     * Is this model valid for saving
     *
     * You can notify error by using addError or check methods
     *
     * @see Storm_Model_Abstract::addError
     * @see Storm_Model_Abstract::check
     *
     * @example
     * public function validate() {
     *    $this->check($this->getRole() < 10, 'role should not exceed 10');
     * }
     *
     */
    public function validate()
    {}


    /**
     * Try to validate the model. If errors found, return false
     * @see Storm_Model_Abstract::validate
     * @return bool
     */
    public function isValid()
    {
        $this->_errors = [];
        $this->validate();

        return !$this->hasErrors();
    }


    /**
     * Return true if contains some errors (do not try to validate)
     * @see Storm_Model_Abstract::validate
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->getErrors()) > 0;
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;

    }


    /**
     * @param string $attribute
     * @param string $error
     */
    public function addAttributeError($attribute, $error)
    {
        $this->_errors[$attribute] = $error;
    }


    /**
     * @param string $error
     */
    public function addError($error)
    {
        $this->_errors[] = $error;
    }


    /**
     * @param bool $condition
     * @param string $error
     */
    public function check($condition, $error)
    {
        if (!$condition) {
            $this->addError($error);
        }
        return $this;
    }


    /**
     * @param string $attribute
     * @param bool $condition
     * @param string $error
     */
    public function checkAttribute($attribute, $condition, $error)
    {
        if (!$condition) {
            $this->addAttributeError($attribute, $error);
        }
        return $this;
    }


    /**
     * Validate an attribute with a validator
     *
     * @param $name string attribute name
     * @param $validator_class string
     * @param $message string
     * @return Storm_Model_Abstract
     */
    public function validateAttribute($name, $validator_class, $message=null)
    {
        $validator = new $validator_class();
        $valid = $validator->isValid($this->_get($name));
        if ($message)
            return $this->checkAttribute($name, $valid, $message);

        foreach($validator->getMessages() as $message)
            $this->checkAttribute($name, $valid, $message);

        return $this;
    }


    /**
     * Return an associative array with attribute
     * name as $key and its value.
     *
     * Used by Loader while saving in order to build the
     * SQL query.
     *
     * @return array
     */
    public function attributesToArray()
    {
        $attributes = [];

        $all_attributes = array_merge($this->_default_attribute_values,
                                      $this->_attributes);

        foreach ($all_attributes as $name => $value) {
            $method = 'get'.$this->attributeNameToAccessor($name);

            if (method_exists($this, $method)) {
                $attributes[$name] = $this->$method();

            } else {
                $attributes[$name] = $value;
            }
        }

        return $attributes;
    }


    /**
     * Return field $_attributes
     *
     * @return array
     */
    public function getRawAttributes()
    {
        return array_merge($this->_default_attribute_values,
                           $this->_attributes);
    }


    /**
     * Return field $_attributes_in_db
     *
     * @return array
     */
    public function getRawDbAttributes()
    {
        return $this->_attributes_in_db;
    }


    /**
     * Return an associative array with attribute
     * name as $key and its value.
     *
     * This method may be redefined in subclasses
     * in order to provide some coupling with Zend_Form::populate()
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributesToArray();
    }


    /**
     * Returns true if this instance has not been loaded from the
     * database or saved.
     *
     * @return bool
     */
    public function isNew()
    {
        return (bool)(!array_key_exists('id', $this->_attributes) ||
                      $this->_attributes['id'] === null);
    }


    public function delete()
    {
        $this->beforeDelete();
        $this->_associations->delete($this);
        Events::getInstance()->notify(new Delete($this));

        if ($this->isNew() || $this->getLoader()->delete($this))
            $this->afterDelete();
    }


    /**
     * @param string $id
     * @return Storm_Model_Abstract
     */
    public function setId($id)
    {
        if ($this->_table_primary != null) {
            $this->_set(strtolower($this->_table_primary), $id);
        }

        //return $this->_set('id', $id); //perfs
        $this->_attributes['id'] = $id;

        return $this;
    }


    /**
     * @param string $field
     * @return string
     */
    public function getIdFieldForDependent($field)
    {
        if (($association = $this->_associations->at($field))
            && $association->isBelongsTo())
            return $association->getReferencedIn();

        return $field . '_id';
    }


    /**
     * @param string $attribute
     * @return bool
     */
    public function isAttributeEmpty($attribute)
    {
        if (!array_key_exists($attribute, $this->_attributes))
            return true;

        $value = $this->_get($attribute);
        return empty($value);
    }


    /**
     * Main purpose is to setup generic getters and setters:
     *
     * $car->getColor();
     * $car->setColor('red');
     * $car->addWheel($w = new Wheel());
     * $car->removeWheel($w);
     * $car->getWheels()   //return an array containing instances;
     * $car->setWheels(array(new Wheel(), new Wheel(), new Wheel(), new Wheel()))
     * $car->hasWheels()   //return true if attribute wheels not empty;
     *
     *
     * @param string $method
     * @param array $args
     * @return Storm_Model_Abstract
     * @throws Exception
     */
    public function __call($method, $args)
    {
        return $this->_associations->handleCall($this, $method, $args);
    }


    /**
     * See __call
     *
     * ex:
     * $car->perform('getColor')
     * $car->perform('setColor', ['red'])
     *
     * @param string $method
     * @param array $args
     * @return Storm_Model_Abstract
     * @throws Exception
     */
    public function perform($method, $args = [])
    {
        if (!preg_match('/(get|set|has|numberOf)(\w+)/', $method, $matches))
            throw new Exception('Tried to call unknown method '.get_class($this).'::'.$method);

        $attribute = $this->_accessorToAttributeName($matches[2]);

        switch ($matches[1]) {
            case 'get':
                return $this->_get($attribute);
            case 'set':
                $this->_set($attribute, $args[0]);
                return $this;
            case 'has':
                return $this->_has($attribute);
            case 'numberOf':
                return $this->_numberOf($attribute);
        }

        return $this;
    }


    /**
     * @param string $field
     * @return array
     */
    public function descriptionOf($field)
    {
        if (method_exists($this, $method = 'descriptionOf'.$field))
            return ['callback' => [$this, $method]];

        if (isset($this->_has_many[$field]))
            return $this->_has_many[$field];

        if (isset($this->_belongs_to[$field]))
            return $this->_belongs_to[$field];

        return null;
    }


    public function hasManyRelationships()
    {
        return $this->_associations->hasManyAssociations();
    }


    /**
     * Return true if $attribute not empty
     *
     * @param String $attribute
     * @return bool
     */
    public function _has($attribute)
    {
        $dependent = $this->callGetterByAttributeName($attribute);
        return !empty($dependent);
    }


    /**
     * Update with a [name] => value formatted array
     *
     * @param Array $datas
     * @return Storm_Model_Abstract
     */
    public function updateAttributes(Array $datas)
    {
        foreach($datas as $name => $value) {
            $method = 'set'.$this->attributeNameToAccessor($name);
            $this->$method($value);
        }

        return $this;
    }


    /**
     * Set initial attribute values with a [name] => value formatted array
     * Used by Loader::newFromRow
     * The difference with updateAttributes is that it doesn't call
     * setAttributeName(...) magic.
     *
     * So we are sure that attributes are exactly those from db.
     *n
     * @param Array $datas
     * @return Storm_Model_Abstract
     */
    public function initializeAttributes($datas)
    {
        $this->_attributes_in_db = $this->_attributes = array_merge($this->_attributes,
                                                                    array_change_key_case($datas));

        return $this;
    }


    /**
     * @param string $name
     * @return boolean true when attribute has changed from initial value in db
     */
    public function hasChangedAttribute($name)
    {
        if ($this->isNew() ||
            null === $name ||
            !isset($this->_attributes[$name]))
            return false;

        if (!isset($this->_attributes_in_db[$name]))
            return true;

        return $this->_attributes_in_db[$name] != $this->_attributes[$name];
    }


    public function hasChange()
    {
        foreach(array_keys($this->toArray()) as $name) {
            if($this->hasChangedAttribute($name))
                return true;
        }

        return false;
    }


    /**
     * UserId -> user_id
     *
     * @param string $accessor
     * @return string
     */
    protected function _accessorToAttributeName($accessor)
    {
        return Inflector::underscorize($accessor);
    }


    /**
     * user_id -> UserId
     *
     * @param string $accessor
     * @return string
     */
    public function attributeNameToAccessor($name)
    {
        return Inflector::camelize($name);
    }


    /**
     * @return boolean true if attribute exists
     */
    public function isAttributeExists($field)
    {
        return
            array_key_exists($field, $this->_attributes) or
            array_key_exists($field, $this->_has_many) or
            array_key_exists($field, $this->_belongs_to) or
            $this->_associations->at($field) or
            $this->hasDefaultValueForAttribute($field);
    }


    /**
     * Return the value of attribute $field. If cannot find it, raise Exception.
     * @param string $field
     * @return mixed
     * @throws Storm_Model_Exception
     */
    protected function _get($field)
    {
        if (isset($this->_attributes[$field])
            || array_key_exists($field, $this->_attributes))
            return $this->_attributes[$field];

        if ($this->hasDefaultValueForAttribute($field))
            return $this->getDefaultValueForAttribute($field);

        throw new Exception(
            sprintf('Tried to call unknown method %s::get%s',
                    get_class($this),
                    $this->attributeNameToAccessor($field)));
    }


    /**
     * Create compatibility with accessor like $miles->instrument.
     * ex:
     *    $miles->setInstrument("trumpet");
     *    assert("trumpet" === $miles->getTrumpet())
     *    assert("trumpet" === $miles->trumpet)
     *
     * If attribute not defined, returns null for compatibility purpose
     * with legacy code - Remember STORM is intended to get
     * easily into messy crappy code. Sorry.
     *
     *     assert(null ===  $miles->zork)
     *
     * @param string $field name of the attribute / field
     * @return mixed the value of attribute or null if does not exist.
     */
    public function __get($field)
    {
        if (array_key_exists($key = strtolower($field), $this->_attributes) ||
            array_key_exists($key = strtoupper($field), $this->_attributes))
            return $this->_attributes[$key];

        return null;
    }


    /**
     * Tells whether a default value is defined for an attribute
     * @param string $field name of the attribute
     * @return boolean true if a default value is defined for this attribute. Otherwise false.
     */
    public function hasDefaultValueForAttribute($field)
    {
        return array_key_exists($field, $this->_default_attribute_values);
    }


    /**
     * Return the default value for given attribute
     * @param string $field name of the attribute
     * @return mixed the default value defined
     */
    public function getDefaultValueForAttribute($field)
    {
        return $this->_default_attribute_values[$field];
    }


    /**
     * @param string $field
     * @param mixed $value
     * @return Storm_Model_Abstract
     */
    public function __set($field, $value)
    {
        $method = 'set'.strtolower($field);
        return $this->$method($value);
    }


    /**
     * @param string $attribute
     */
    public function callGetterByAttributeName($attribute)
    {
        return call_user_func(array($this, 'get'.$this->attributeNameToAccessor($attribute)));
    }


    /**
     * @param string $attribute
     * @param mixed $value
     */
    public function callSetterByAttributeName($attribute, $value)
    {
        return call_user_func([$this, 'set'.$this->attributeNameToAccessor($attribute)], $value);
    }


    /**
     * @param string $attribute
     */
    public function callAdderByAttributeName($attribute, $value)
    {
        $method = 'add' . $this->attributeNameToAccessor(Inflector::singularize($attribute));
        return call_user_func([$this, $method], $value);
    }


    /**
     * @param string $field
     * @param mixed $value
     * @return Storm_Model_Abstract
     */
    protected function _set($field, $value)
    {
        $this->_attributes[$field] = $value;
        return $this;
    }


    public function __toString()
    {
        return get_class($this).'(id='.(string)$this->getId().')';
    }


    public function acceptHierarchyVisitor($visitor)
    {
        $my_name = get_class($this);
        $this->_associations->eachDo(function($association) use($my_name, $visitor) {
            if (!$model_class = $association->getOption('model'))
                return;

            $visitor->visit(static::getLoaderFor($model_class)->newInstance(),
                            $my_name, $association->getName());
        });
    }


    public function dotGraph()
    {
        $visitor = new HierarchyVisitor();
        $this->acceptHierarchyVisitor($visitor);

        return $visitor->asDot();
    }


    public function isFixedId()
    {
        return $this->_fixed_id;
    }
}
