<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2019- Patrick Barroca and contributors, see CONTRIBUTORS.txt
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;

use Storm\Model\Table\CommandVisitor;


abstract class CreateVisitor extends CommandVisitor
{
    protected
        $_table,
        $_fields = [],
        $_indexes = [],
        $_options = [];


    public function __call($name, $args)
    {
        if ('visit' != substr($name, 0, 5))
            throw new \RuntimeException('Call to unknown method ' . $name);

        $map = $this->_simpleTypeMap();

        $type = substr($name, 5);
        if (array_key_exists($type, $map)) {
            $args[] = $map[$type];
            return call_user_func_array([$this, '_visitField'], $args);
        }

        throw new \RuntimeException('Call to unknown method ' . $name);
    }


    /**
     * @return array ['Type name' => 'column type']
     */
    abstract protected function _simpleTypeMap();


    protected function _visitField($name, $nullable, $index, $default, $type)
    {
        $definition = $this->_define($name, $type, $nullable);
        $this->_fields[] = implode(' ', array_filter($definition));

        if (null !== $index)
            $this->_indexes[$name] = $index;
    }


    public function visitTable($name)
    {
        $this->_table = $name;
    }


    public function visitEngine($name)
    {
    }


    public function visitInteger($name, $nullable, $index, $default, $increment)
    {
    }


    abstract protected function _define($name, $type, $nullable);



    public function toSql()
    {
        return sprintf(
            'create table %s (%s) %s',
            $this->quoteIdentifier($this->_table), $this->_definitions(), $this->_options()
        );
    }


    protected function _definitions()
    {
        $definitions = [$this->_fields(), $this->_indexes()];
        return implode(', ', array_filter($definitions));
    }


    protected function _fields()
    {
        return implode(', ', $this->_fields);
    }


    protected function _indexes()
    {
        $indexes = [];
        foreach($this->_indexes as $name => $type)
            $indexes[] = $type . ' KEY ' . $this->quoteIdentifier($name)
            . ' (' . $this->quoteIdentifier($name). ')';

        return implode(', ', $indexes);
    }


    protected function _options()
    {
        return implode(' ', $this->_options);
    }
}
