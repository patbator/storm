<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence\Mysqli;

use Storm\Persistence\Connection as BaseConnection;
use Storm\Model\Table\Select;
use Storm\Model\Table\LiteralSelect;
use Storm\Model\Table\Insert;
use Storm\Model\Table\Update;
use Storm\Model\Table\Delete;
use Storm\Model\Table\Create;
use Storm\Model\Table\Drop;


class Connection extends BaseConnection
{
    /** @category testing */
    public function setMysqli($mysqli)
    {
        $this->_client = $mysqli;
        return $this;
    }


    public function fetchOne($select)
    {
        return $this->query($select)->fetch_assoc();
    }


    public function fetchMany($select)
    {
        $result = $this->query($select);
        $rows = [];
        while ($row = $result->fetch_assoc())
            $rows[] = $row;

        return $rows;
    }


    public function lastError()
    {
        return $this->_client
            ? $this->_client->error
            : '';
    }


    public function lastConnectError()
    {
        return $this->_client
            ? $this->_client->connect_error
            : '';
    }


    public function lastInsertId()
    {
        return $this->_client
            ? $this->_client->insert_id
            : 0;
    }


    public function affectedRows()
    {
        return $this->_client
            ? $this->_client->affected_rows
            : 0;
    }


    protected function _connect()
    {
        if ($this->_client)
            return true;

        $this->_client = mysqli_init();

        $conf = $this->_configuration;
        foreach($conf->getOptions() as $name => $value)
            $this->_client->options($name, $value);

        $this->_client->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, 1);

        $this->_client->real_connect($conf->getHost(),
                                     $conf->getUser(),
                                     $conf->getPass(),
                                     $conf->getDatabase(),
                                     $conf->getPort());

        return $this->_client !== false && !mysqli_connect_errno();
    }


    public function quoteValue($value)
    {
        if (is_array($value)) {
            $quoted = [];
            foreach ($value as $one)
                $quoted[] = $this->quoteValue($one);

            return $quoted;
        }

        if (is_null($value))
            return 'null';

        return is_int($value)
            ? $value
            : "'" . $this->_client->real_escape_string($value) . "'";
    }


    public function quoteIdentifier($name)
    {
        return '`' . $name . '`';
    }


    public function visitorFor($command)
    {
        if ($command instanceof Select || $command instanceof LiteralSelect)
            return new \Storm\Persistence\SelectVisitor($this);

        if ($command instanceof Insert)
            return new \Storm\Persistence\InsertVisitor($this);

        if ($command instanceof Update)
            return new \Storm\Persistence\UpdateVisitor($this);

        if ($command instanceof Delete)
            return new \Storm\Persistence\DeleteVisitor($this);

        if ($command instanceof Create)
            return new CreateVisitor($this);

        if ($command instanceof Drop)
            return new \Storm\Persistence\DropVisitor($this);

        throw new \RuntimeException('Unsupported command object in this driver : ' . get_class($command));
    }
}
