<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;


class VolatileOrderClause
{
    protected
        $_fieldname,
        $_descendant = false,
        $_next_clause = null;


    public static function parseOrderClause($order)
    {
        $clauses = array_reverse(array_filter(explode(',', strtolower($order))));

        $first_clause = new static(array_shift($clauses));
        foreach($clauses as $clause) {
            $first_clause = new static($clause, $first_clause);
        }
        return $first_clause;
    }


    public function __construct($order, $next_clause = null)
    {
        $description = explode(' ', trim($order));

        $this->_fieldname = trim(array_shift($description));
        $this->_descendant = (array_shift($description) == 'desc');
        $this->_next_clause = $next_clause;
    }


    public function compare($a, $b)
    {
        $first = (array_key_exists($this->_fieldname, $a)) ? $a[$this->_fieldname] : '';
        $last = (array_key_exists($this->_fieldname, $b)) ? $b[$this->_fieldname] : '';

        if ($this->_descendant) {
            $temp = $first;
            $first = $last;
            $last = $temp;
        }

        $result = (is_int($first))
            ? $this->int_compare((int)$first, (int)$last)
            : $this->string_compare($first, $last);

        return (($result === 0) && $this->_next_clause)
            ? $this->_next_clause->compare($a, $b)
            : $result;
    }


    public function int_compare($a , $b)
    {
        return ($a == $b) ? 0 : (($a < $b) ? -1 : 1);
    }


    public function string_compare($a, $b)
    {
        return strcmp($a, $b);
    }
}
