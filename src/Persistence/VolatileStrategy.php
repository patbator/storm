<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;


class VolatileStrategy extends AbstractStrategy {
    protected
        $_instances = [],
        $desc_order = false,
        $special_select_fields = ['order', 'limit', 'limitpage', 'where'];


    public function findAll($select = null)
    {
        if (null === $filtered_select = $this->extractRoleAndModel(is_array($select)
                                                                   ? array_change_key_case($select)
                                                                   : []))
            return [];

        $order = isset($filtered_select['order']) ? $filtered_select['order'] : '';
        $limit = isset($filtered_select['limit']) ? $filtered_select['limit'] : '';

        $instances = $this->getInstancesArray();
        $page_size=0;
        if (isset($filtered_select['limitpage'])) {
            list($page, $page_size) = $filtered_select['limitpage'];
            if ($page > 0) $page -= 1;
        }

        foreach($this->special_select_fields as $field)
            unset($filtered_select[$field]);

        $negations = [];
        foreach($filtered_select as $k => $v) {
            if (' not' == substr($k, -4)) {
                $negations[substr($k, 0, strlen($k) -4)] = $v;
                unset($filtered_select[$k]);
            }
        }

        $values = array_values(
            array_filter(
                $instances,
                function($instance) use($filtered_select) {
                    return $this->containsAllAttributes($instance, $filtered_select);
                }));

        if (!empty($negations))
            $values = array_values(array_filter($values,
                                                function($instance) use($negations) {
                                                    return !$this->containsAllAttributes($instance, $negations);
                                                }));

        $values = $this->ordered($values, $order);
        $values = $this->limited($values, $limit);

        if ($page_size>0)
            $values = array_slice($values, $page * $page_size, $page_size);

        return array_map([$this->_loader, 'newFromRow'],
                         $values);
    }


    protected function extractRoleAndModel($select)
    {
        if (array_key_exists('role', $select) && array_key_exists('model', $select)) {
            $model = $select['model'];
            $role = $select['role'];
            unset($select['model']);
            unset($select['role']);

            if ($model->isNew()) return null;

            $field = $this->_loader->getIdFieldForDependent($role);
            $select[$field]=$model->getId();
        }

        if (array_key_exists('scope', $select)) {
            $select = array_merge($select, array_change_key_case($select['scope']));
            unset($select['scope']);
        }

        return $select;
    }


    protected function getInstancesArray()
    {
        return
            array_values(
                array_map(
                    function($model) {return $model->getRawAttributes();},
                    $this->_instances));
    }


    public function findAllBy($args)
    {
        return $this->findAll($args);
    }


    protected function compareFunction($order)
    {
        $first_clause = VolatileOrderClause::parseOrderClause($order);

        return function($a, $b) use ($first_clause) {
            return $first_clause->compare($a, $b);
        };
    }


    public function ordered($result, $select)
    {
        if (! ($select && $result) )
            return $result;

        if (is_array($select))
            $select = implode(',', $select);

        usort($result, $this->compareFunction($select));
        return $result;
    }


    private function limited($result, $limit)
    {
        if ('' == $limit)
            return $result;

        if (false === strpos($limit, ','))
            return array_slice($result, 0, (int)$limit);

        $parts = explode(',', $limit);
        return array_slice($result, (int)$parts[0], (int)$parts[1]);
    }


    public function containsAllAttributes($model, $select)
    {
        foreach ($select as $key => $value) {
            if (!array_key_exists($key, $model)
                || ($model[$key]!=$value
                    && !(is_array($value) && in_array($model[$key], $value))))
                return false;
        }

        return true;
    }


    public function find($id)
    {
        if (!isset($this->_instances[$id]))
            return null;

        return $this->_instances[$id]->setId($id);
    }


    public function insert($data)
    {
        $instance = $this->_loader->newInstance();
        $instance->updateAttributes($data);

        $id = $instance->isFixedId() ?
            $instance->callGetterByAttributeName($this->_loader->getIdField())
            : $this->lastInsertId() + 1;

        $instance->setId($id);
        $this->_instances[$id]=$instance;

        return true;
    }


    public function lastInsertId()
    {
        if (sizeof($this->_instances)<1)
            return 0;

        return max(array_keys($this->_instances));
    }


    public function update($data,$id)
    {
        if (!isset($this->_instances[$id]))
            $instance = $this->_loader->newInstance();
        else
            $instance = $this->_instances[$id];

        $instance
            ->updateAttributes($data)
            ->setId($id);
        $this->_instances[$id]=$instance;

        return $instance;
    }


    public function delete($model)
    {
        unset($this->_instances[$model->getId()]);
        return true;
    }


    public function countBy($args)
    {
        return sizeof($this->findAll($args));
    }


    public function isVolatile()
    {
        return true;
    }
}
