<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2019- Patrick Barroca and contributors, see CONTRIBUTORS.txt

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence\Sqlite;

use Storm\Persistence\CreateVisitor as BaseVisitor;


class CreateVisitor extends BaseVisitor
{
    protected function _simpleTypeMap()
    {
        return [
            'Character' => 'TEXT',
            'Date'      => 'TEXT',
            'Blob'      => 'BLOB',
            'Text'      => 'TEXT',
        ];
    }


    public function visitInteger($name, $nullable, $index, $default, $increment)
    {
        $definition = $this->_define($name, 'INTEGER');
        if ($increment)
            $definition[] = 'PRIMARY KEY';

        $this->_fields[] = implode(' ', $definition);

        if ($index)
            $this->_indexes[$name] = $index;
    }


    protected function _define($name, $type, $nullable)
    {
        return [
            $this->quoteIdentifier($name),
            $type,
        ];
    }
}
