<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2019- Patrick Barroca and contributors, see CONTRIBUTORS.txt

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;


abstract class Connection
{
    protected
        $_configuration,
        $_listener,
        $_client;


    public function __construct($configuration)
    {
        $this->_configuration = $configuration;
    }


    public function setListener($listener)
    {
        $this->_listener = $listener;
        return $this;
    }


    public function query($command)
    {
        if (!$this->_connect())
            throw new \RuntimeException($this->lastConnectError());

        $visitor = $this->visitorFor($command);
        $command->acceptVisitor($visitor);

        if (false === $result = $this->_client->query($sql = $visitor->toSql()))
            throw new \RuntimeException($this->lastError());

        $this->_notify($sql);
        return $result;
    }


    protected function _notify($sql)
    {
        if ($listener = $this->_listener)
            $listener($sql);

        return $this;
    }


    /**
     * Try to connect with underlying driver
     * Store connection into $this->_client
     *
     * @return bool weither it succeed or not
     */
    abstract protected function _connect();


    abstract public function fetchOne($select);


    abstract public function fetchMany($select);


    abstract public function lastError();


    abstract public function lastConnectError();


    abstract public function lastInsertId();


    abstract public function affectedRows();


    abstract public function quoteValue($value);


    abstract public function quoteIdentifier($name);


    /**
     * Given a \Storm\Model\Table\Command
     * answers a visitor able to translate it to SQL dialect
     *
     * @param \Storm\Model\Table\Command $command
     * @return \Storm\Persistence\CommandVisitor
     * @throws \RuntimeException when command is not supported by this connection
     */
    abstract public function visitorFor($command);
}
