<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2019- Patrick Barroca and contributors, see CONTRIBUTORS.txt

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;


class SqlStrategy extends AbstractStrategy
{
    public function getTable()
    {
        if (isset($this->_table))
            return $this->_table;

        return $this->_table = new Table($this->_loader);
    }


    public function findAll($select = null)
    {
        if ($rowset = $this->getTable()->findAll($select))
            return array_map([$this->_loader, 'newFromRow'],
                             $rowset);

        return [];
    }


    public function find($id)
    {
        return ($row = $this->getTable()->find($id))
            ? $this->_loader->newFromRow($row)
            : null;
    }


    public function findAllBy($args)
    {
        return $this->findAll($args);
    }


    public function insert($data)
    {
        return $this->getTable()->insert($data);
    }


    public function lastInsertId()
    {
        return $this->getTable()->lastInsertId();
    }


    public function update($data, $id)
    {
        return $this->getTable()->update($data, $id);
    }


    public function delete($model)
    {
        return $this->getTable()->delete($model->getId());
    }


    public function countBy($args)
    {
        return $this->getTable()->countBy($args);
    }


    public function isVolatile()
    {
        return false;
    }


    public function create($callback)
    {
        return $this->getTable()->create($callback);
    }


    public function drop()
    {
        return $this->getTable()->drop();
    }
}
