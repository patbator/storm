<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2019- Patrick Barroca and contributors, see CONTRIBUTORS.txt

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;

use Storm\Model\Table\CommandVisitor;


class SelectVisitor extends CommandVisitor
{
    use WhereVisitor;

    protected
        $_literal_sql,
        $_table,
        $_fields = [],
        $_order = [],
        $_limit_count,
        $_limit_offset;


    public function visitLiteralSql($sql)
    {
        $this->_literal_sql = $sql;
        return $this;
    }


    public function visitFields($fields)
    {
        $this->_fields = $fields;
        return $this;
    }


    public function visitTable($table)
    {
        $this->_table = $this->quoteIdentifier($table);
        return $this;
    }


    public function visitLimit($count, $offset)
    {
        $this->_limit_count = (int)$count;
        $this->_limit_offset= (int)$offset;
        return $this;
    }


    public function visitOrderBy($field, $direction)
    {
        $this->_order[] = $this->quoteIdentifier($field) . ' ' . $direction;
        return $this;
    }


    public function toSql()
    {
        if ($this->_literal_sql)
            return $this->_literal_sql;

        $sql = sprintf('select %s from %s',
                       $this->_fields,
                       $this->_table);

        $sql .= $this->whereToSql();

        if ($this->_order)
            $sql .= ' order by ' . implode(', ', $this->_order);

        if ($this->_limit_count)
            $sql .= ' limit '
                . ($this->_limit_offset ? ($this->_limit_offset . ',') : '')
                . $this->_limit_count ;

        return $sql;
    }
}
