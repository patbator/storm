<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;


class Configuration
{
    const
        PARAM_HOST    = 'host',
        PARAM_PORT    = 'port',
        PARAM_DATABASE= 'database',
        PARAM_USER    = 'user',
        PARAM_PASS    = 'pass',
        PARAM_OPTIONS = 'options';

    protected  $_params = [];


    public function __construct($params=[])
    {
        foreach($params as $k => $v)
            $this->setParam($k, $v);
    }


    public function setParam($name, $value)
    {
        $this->_params[$name] = $value;
        return $this;
    }


    public function getParam($name, $default='')
    {
        return array_key_exists($name, $this->_params)
            ? $this->_params[$name]
            : $default;
    }


    public function getHost()
    {
        return $this->getParam(static::PARAM_HOST);
    }


    public function getPort()
    {
        return $this->getParam(static::PARAM_PORT, null);
    }


    public function getDatabase()
    {
        return $this->getParam(static::PARAM_DATABASE);
    }


    public function getUser()
    {
        return $this->getParam(static::PARAM_USER);
    }


    public function getPass()
    {
        return $this->getParam(static::PARAM_PASS);
    }


    public function getOptions()
    {
        return $this->getParam(static::PARAM_OPTIONS, []);
    }
}
