<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;


class Connections
{
    const DEFAULT_NAME = 'default';

    protected static $_instance;

    public static function getInstance()
    {
        if (!static::$_instance)
            static::$_instance = new static();

        return static::$_instance;
    }


    /** @category testing */
    public static function setInstance($instance)
    {
        static::$_instance = $instance;
    }


    protected $_connections = [];


    public function setDefault($connection)
    {
        return $this->atPut(static::DEFAULT_NAME, $connection);
    }


    public function getDefault()
    {
        return $this->get(static::DEFAULT_NAME, reset($this->_connections));
    }


    public function atPut($name, $value)
    {
        $this->_connections[$name] = $value;
        return $this;
    }


    public function get($name, $default)
    {
        if (array_key_exists($name, $this->_connections))
            return $this->_connections[$name];

        return $default;
    }
}
