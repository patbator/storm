<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2019- Patrick Barroca and contributors, see CONTRIBUTORS.txt

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace Storm\Persistence;

use Storm\Persistence\Connections;
use Storm\Model\Table\Select;
use Storm\Model\Table\LiteralSelect;
use Storm\Model\Table\Insert;
use Storm\Model\Table\Update;
use Storm\Model\Table\Delete;
use Storm\Model\Table\Create;
use Storm\Model\Table\Drop;


class Table
{
    protected
        $_connection,
        $_name,
        $_id_field;


    public  function __construct($loader)
    {
        $this->_name = $loader->getTableName();
        $this->_id_field = $loader->getIdField();
        $this->_connection = Connections::getInstance()->getDefault();
    }


    public function find($id)
    {
        $select = $this->select();
        $select
            ->where()->equalsTo($this->_id_field, $id)
            ->limit()->toOne();

        return $this->_connection->fetchOne($select);
    }


    public function findAll($params=[])
    {
        $select = $this->paramsToSelect($params);
        return $this->_connection->fetchMany($select);
    }


    public function countBy($params)
    {
        if (!$params)
            $params = [];

        $select = $this->paramsToSelect($params)->withCount($this->_id_field);

        return (int)($this->_connection->fetchOne($select)['numberOf']);
    }


    public function paramsToSelect($params)
    {
        if (is_string($params))
            return $this->literalSelect($params);

        $select = $this->select();
        foreach($params as $field => $value)
            $this->paramToSelect($field, $value, $select);

        return $select;
    }


    public function paramToSelect($field, $value, $select)
    {
        if ('order' == $field && is_array($value)) {
            foreach($value as $field => $direction)
                $select->order()->by($field, $direction);

            return;
        }

        if ('order' == $field)
            return $select->order()->by((string)$value);

        if ('limit' == $field)
            return is_array($value)
                ? $select->limit()->countStartingAt($value[0], $value[1])
                : $select->limit()->to((int)$value);

        if ('where' == $field)
            return $select->where()->literal($value);

        $negated = (' not' == substr($field, -4));
        $field = $negated ? substr($field, 0, strlen($field) - 4): $field;

        $where = $select->where();
        if (null === $value)
            return $negated ? $where->isNotNull($field) : $where->isNull($field);

        if (is_array($value))
            return $negated ? $where->isNotIn($field, $value) : $where->isIn($field, $value);

        $negated ? $where->notEqualsTo($field, $value) : $where->equalsTo($field, $value);
    }


    public function select()
    {
        return (new Select())->from($this->_name);
    }


    public function literalSelect($sql)
    {
        return new Literalselect($sql);
    }


    public function insert($data)
    {
        $insert = (new Insert())->into($this->_name);
        $fields = $insert->fields();
        foreach($data as $field => $value)
            $fields->add($field, $value);

        return $this->_connection->query($insert);
    }


    public function lastInsertId()
    {
        return $this->_connection->lastInsertId();
    }


    public function update($data, $id)
    {
        $update = (new Update())->table($this->_name);

        $fields = $update->fields();
        foreach($data as $field => $value)
            if ($field != $this->_id_field)
                $fields->add($field, $value);

        $update->where()->equalsTo($this->_id_field, $id);

        return $this->_connection->query($update);
    }


    public function delete($id)
    {
        $delete = (new Delete())->table($this->_name);
        $delete->where()->equalsTo($this->_id_field, $id);

        return $this->_connection->query($delete)
            ? $this->_connection->affectedRows()
            : 0;
    }


    public function create($callback)
    {
        $create = $callback((new Create())->table($this->_name));
        return $this->_connection->query($create);
    }


    public function drop()
    {
        return $this->_connection->query((new Drop())->table($this->_name));
    }
}
