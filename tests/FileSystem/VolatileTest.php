<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2023- Patrick Barroca
Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest\FileSystem;

use PHPUnit\Framework\TestCase;
use Storm\FileSystem\Volatile;


class VolatileTest extends TestCase
{
  protected $_fs;

  protected function setUp(): void
  {
    parent::setUp();
    $this->_fs = (new Volatile())
      ->mkdir('/public/bokeh/skins')
      ->cd('/public/bokeh/skins')
      ->mkdir('dark')
      ->mkdir('light')
      ->touch('.htaccess')

      ->cd('light')
      ->touch('style.css')

      ->cd('/')
      ->mkdir('etc/')
      ->mkdir('etc/php')
      ->touch('etc/php/php.ini')
      ->touch('etc/php/php.old')
      ->rm('etc/php/php.old')

      ->cd('/etc')
      ->touch('/var/sql.lck')
      ->mkdir('/var/www')

      ->filePutContents('/var/notes.txt', 'buy some butter');
  }


  /** @test */
  public function notesDotTxtGetContentsShouldAnswersBuySomeButter()
  {
    $this->assertEquals(
      'buy some butter',
      $this->_fs->fileGetContents('/var/notes.txt')
    );
  }


  /** @test */
  public function notesTxtFileGetSizeTxtShouldAnswersFifteen()
  {
    $this->assertEquals(
      15,
      $this->_fs->fileGetSize('/var/notes.txt')
    );
  }


  /** @test */
  public function ghostGetContentsShouldAnswersNull()
  {
    $this->assertNull($this->_fs->fileGetContents('ghost.txt'));
  }


  /** @test */
  public function ghostFileGetSizeShouldAnswersZero()
  {
    $this->assertSame(0, $this->_fs->fileGetSize('ghost.txt'));
  }


  public function validPaths()
  {
    return [
      ['/etc'],
      ['php'],
      ['/etc/php'],
      ['/public/bokeh/skins'],
      ['/public/bokeh/skins/light/'],
      ['/public/bokeh/skins/dark'],
      ['/var/www'],
      ['php/php.ini'],
      ['./php/php.ini'],
      //      ['../etc/php/php.ini'],
      ['/public/bokeh/skins/light/style.css']
    ];
  }


  /**
   * @dataProvider validPaths
   * @test */
  public function validPathShouldExists($path)
  {
    $this->assertTrue($this->_fs->fileExists($path));
  }


  public function invalidPaths()
  {
    return [
      ['/zork'],
      ['/php'],
      ['/var/php/zork'],
      ['var'],
      ['/etc/zork.ini'],
      ['/etc/php/php.old']
    ];
  }


  /**
   * @dataProvider invalidPaths
   * @test */
  public function invalidPathShouldNotExists($path)
  {
    $this->assertFalse($this->_fs->fileExists($path));
  }


  /** @test */
  public function bokehSkinsShouldHaveDarkAndLightSubdirs()
  {
    $dirs = $this->_fs->entryAt('/public/bokeh/skins')->collect(
      function ($entry) {
        return $entry->getName();
      }
    );
    $this->assertEquals(['dark', 'light', '.htaccess'], $dirs);
  }


  /** @test */
  public function bokehSkinsShouldHaveOnlyDarkAndLightSubdirs()
  {
    $dirs = $this->_fs->directoryNamesAt('/public/bokeh/skins');
    $this->assertEquals(['dark' => 'dark', 'light' => 'light'], $dirs);
  }
}
