<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use StormTest\Mock\Cat;
use StormTest\Mock\Dog;
use StormTest\Mock\User;

class UserBondTest extends ModelTestCase
{
  public $bond, $felix, $calypso, $pluto;

  protected function setUp(): void
  {
    $this->bond = $this->fixture(
      User::class,
      [
        'id' => 3,
        'name' => 'Bond',
        'matriculation' => '007',
        'first_name' => 'James'
      ]
    );

    $this->bond->FAVORITE_COCKTAIL = 'Martini Dry';

    $this->felix = $this->fixture(
      Cat::class,
      [
        'id' => 1,
        'name' => 'Felix',
        'owner' => $this->bond
      ]
    );

    $this->calypso = $this->fixture(
      Cat::class,
      [
        'id' => 2,
        'name' => 'Calypso',
        'owner' => $this->bond
      ]
    );

    $this->pluto = $this->fixture(
      Dog::class,
      [
        'id' => 1,
        'name' => 'Pluto',
        'owner' => $this->bond
      ]
    );
  }


  /** @test */
  public function nameShouldBeBond()
  {
    $this->assertEquals('Bond', $this->bond->getName());
  }


  /** @test */
  public function matriculationShouldBe007()
  {
    $this->assertEquals('007', $this->bond->getMatriculation());
  }


  /** @test */
  public function firstNameShouldBeJames()
  {
    $this->assertEquals('James', $this->bond->getFirstName());
  }


  /** @test */
  public function favoriteCocktailShouldBeMartiniDry()
  {
    $this->assertEquals('Martini Dry', $this->bond->getFavoriteCocktail());
  }


  /** @test */
  public function favoriteCocktailPropertyShouldBeMartiniDry()
  {
    $this->assertEquals('Martini Dry', $this->bond->FAVORITE_COCKTAIL);
  }


  /** @test */
  public function unknownPropertyShouldBeNull()
  {
    $this->assertNull($this->bond->REALLY_UNKNOWN);
  }


  /** @test */
  public function felixShouldNotHaveChange()
  {
    $this->assertFalse($this->felix->hasChange());
  }


  /** @test */
  public function felixShouldHaveAName()
  {
    $this->assertTrue($this->felix->hasName());
  }


  /** @test */
  public function felixShouldNotHaveDescriptionOfMeals()
  {
    $this->assertNull($this->felix->descriptionOf('meals'));
  }


  /** @test */
  public function toArrayShouldReturnsOneWordAttributes()
  {
    $attributes = $this->bond->toArray();
    $this->assertEquals('007', $attributes['matriculation']);
    $this->assertEquals('Bond', $attributes['name']);
  }


  /** @test */
  public function toArrayShouldReturnsUnderscoredAttributes()
  {
    $attributes = $this->bond->toArray();
    $this->assertEquals('James', $attributes['first_name']);
    $this->assertEquals('Martini Dry', $attributes['favorite_cocktail']);
  }


  /** @test */
  public function toStringShouldReturnClassNameAndId()
  {
    $this->assertEquals(User::class . '(id=3)', $this->bond->__toString());
  }


  /** @test */
  public function animalsShouldReturnAnArrayWithFelixAndCalypso()
  {
    $this->assertEquals(
      [$this->felix, $this->calypso],
      $this->bond->getAnimals()
    );
  }


  /** @test */
  public function numberOfAnimalsShouldReturnTwo()
  {
    $this->assertEquals(2, $this->bond->numberOfAnimals());
  }


  /** @test */
  public function animalsShouldReturnAnArrayWithPlutoIfNotLikeCats()
  {
    $this->bond->setLikeCats(false);
    $this->assertEquals([$this->pluto], $this->bond->getAnimals());
  }


  /** @test */
  public function numberOfAnimalsShouldReturnOneIfNotLikeCats()
  {
    $this->bond->setLikeCats(false);
    $this->assertEquals(1, $this->bond->numberOfAnimals());
  }


  /** @test */
  public function catShouldHaveBelongsToRelationshipWithOwner()
  {
    $this->assertTrue($this->felix->hasBelongsToRelashionshipWith('owner'));
  }


  /** @test */
  public function callAdderByAttributeAnimalShouldAddToCollection()
  {
    $royce = Cat::newInstance(['Royce']);
    $this->bond->callAdderByAttributeName('animals', $royce);
    $this->assertContains($royce, $this->bond->getAnimals());
  }
}
