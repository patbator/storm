<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2023- Patrick Barroca
Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use StormTest\Mock\User;

class AttributeChangeForNewInstanceTest extends ModelTestCase
{
  public $new_user;

  protected function setUp(): void
  {
    $this->new_user = User::newInstance([
      'name' => 'Pat1',
      'first_name' => 'Le Batman1'
    ]);

    $this->new_user->setName('Quentin');
  }


  /** @test **/
  public function classNameShoudBeMockUser()
  {
    $this->assertEquals(
      User::class,
      $this->new_user->getClassName()
    );
  }


  /** @test */
  public function nameShouldNotHaveChanged()
  {
    $this->assertFalse($this->new_user->hasChangedAttribute('name'));
  }


  /** @test */
  public function firstNameShouldNotHaveChanged()
  {
    $this->assertFalse($this->new_user->hasChangedAttribute('first_name'));
  }


  /** @test */
  public function likeCatsShouldNotHaveChanged()
  {
    $this->assertFalse($this->new_user->hasChangedAttribute('like_cats'));
  }


  /** @test */
  public function addedAttributeShouldHaveChanged()
  {
    $this->new_user->setNaissance('1992');
    $this->assertFalse($this->new_user->hasChangedAttribute('naissance'));
  }


  /** @test */
  public function unKnownAttributeShouldNotHaveChanged()
  {
    $this->assertFalse($this->new_user->hasChangedAttribute('color'));
  }


  /** @test */
  public function nullAttributeShouldNotHaveChanged()
  {
    $this->assertFalse($this->new_user->hasChangedAttribute(null));
  }
}



class AttributeChangeForExistingUserTest extends ModelTestCase
{
  public $user;

  protected function setUp(): void
  {
    $this->user = User::newFromRow([
      'id' => 1,
      'name' => 'Pat ',
      'first_name' => 'Le Batman'
    ]);

    $this->user->setName('Quentin');
  }


  /** @test */
  public function nameShouldBeQuentin()
  {
    $this->assertEquals('Quentin', $this->user->getName());
  }


  /** @test */
  public function nameShouldHaveChanged()
  {
    $this->assertTrue($this->user->hasChangedAttribute('name'));
  }


  /** @test */
  public function firstNameShouldNotHaveChanged()
  {
    $this->assertFalse($this->user->hasChangedAttribute('first_name'));
  }


  /** @test */
  public function likeCatsShouldNotHaveChanged()
  {
    $this->assertFalse($this->user->hasChangedAttribute('like_cats'));
  }


  /** @test */
  public function addedAttributeShouldHaveChanged()
  {
    $this->user->setNaissance('1992');
    $this->assertTrue($this->user->hasChangedAttribute('naissance'));
  }


  /** @test */
  public function unKnownAttributeShouldNotHaveChanged()
  {
    $this->assertFalse($this->user->hasChangedAttribute('color'));
  }


  /** @test */
  public function nullAttributeShouldNotHaveChanged()
  {
    $this->assertFalse($this->user->hasChangedAttribute(null));
  }


  /** @test */
  public function modelShouldHaveChange()
  {
    $this->assertTrue($this->user->hasChange());
  }
}
