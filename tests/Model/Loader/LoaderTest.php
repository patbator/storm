<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Model\Loader;

use Storm\Testing\ModelTestCase;
use Storm\Persistence\Connections;
use Storm\Persistence\SqlStrategy;
use Storm\Persistence\Mysqli\Connection;
use StormTest\Mock\User;

abstract class LoaderTestCase extends ModelTestCase
{
  protected
    $_connection,
    $_mysqli,
    $_result,
    $_queries = [];

  protected function setUp(): void
  {
    parent::setUp();

    $this->_result = $this->mock();
    $this->_mysqli = new class($this->_result)
    {
      public $affected_rows, $insert_id;
      protected $_result;

      public function __construct($result)
      {
        $this->_result = $result;
      }

      public function query()
      {
        return $this->_result;
      }

      public function real_escape_string($value)
      {
        return $value;
      }
    };

    $listener = fn ($sql) => $this->_queries[] = $sql;

    $this->_connection = ((new Connection([]))
      ->setMysqli($this->_mysqli)
      ->setListener($listener));

    Connections::getInstance()->setDefault($this->_connection);
    User::defaultTo(SqlStrategy::class);
  }


  protected function tearDown(): void
  {
    Connections::getInstance()->setDefault(null);
    User::defaultToVolatile();
    parent::tearDown();
  }


  protected function assertSql($sql)
  {
    $this->assertContains($sql, $this->_queries, json_encode($this->_queries, JSON_PRETTY_PRINT));
    return $this;
  }
}



class LoaderFindTest extends LoaderTestCase
{
  /** @test */
  public function with42ShouldContainsIdEquals42LimitedToOne()
  {
    $this->_result->whenCalled('fetch_assoc')->answers(['id' => 42, 'name' => 'Tandoori']);
    $user = User::find(42);
    $this->assertSql('select * from `mock_user` where (`id`=42) limit 1');
    $this->assertEquals('Tandoori', $user->getName());
  }
}




class LoaderSaveNewTest extends LoaderTestCase
{
  /** @test */
  public function shouldContainsInsertStatement()
  {
    $this->_result->whenCalled('fetch_assoc')->answers(false);
    $this->_mysqli->insert_id = 3;

    (new User())
      ->setName('Harlock')
      ->setFirstName('Captain')
      ->setCode(null)
      ->save();

    $this->assertSql('insert into `mock_user` (`name`,`first_name`,`like_cats`,`code`) values (\'Harlock\',\'Captain\',\'1\',null)');
  }
}



class LoaderSaveExistingTest extends LoaderTestCase
{
  /** @test */
  public function shouldContainsInsertStatement()
  {
    $this->_result->whenCalled('fetch_assoc')->answers(false);

    (new User())
      ->setId(42)
      ->setName('Harlock')
      ->setFirstName('Captain')
      ->setCode(null)
      ->save();

    $this->assertSql('update `mock_user` set `name`=\'Harlock\', `first_name`=\'Captain\', `like_cats`=\'1\', `code`=null where (`id`=42)');
  }
}




class LoaderDeleteTest extends LoaderTestCase
{
  /** @test */
  public function shouldContainsDeleteStatement()
  {
    $this->_mysqli->affected_rows = 1;

    (new User())
      ->setId(42)
      ->setName('Harlock')
      ->setFirstName('Captain')
      ->setCode(null)
      ->delete();

    $this->assertSql('delete from `mock_user` where (`id`=42)');
  }
}




class LoaderFindAllByTest extends LoaderTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    $this->_result->whenCalled('fetch_assoc')->answers(false);
  }


  /** @test */
  public function withRawSqlShouldExecuteIt()
  {
    $this->assertEquals([], User::findAll('SELECT * FROM USERS'));
    $this->assertSql('SELECT * FROM USERS');
  }


  /** @test */
  public function withSimpleParamsShouldContainsEqualsAndEquals()
  {
    $this->findAllBy([
      'login' => 'captain',
      'password' => 'harlock'
    ])
      ->assertSql('select * from `mock_user` where (`login`=\'captain\') and (`password`=\'harlock\')');
  }


  /** @test */
  public function withSimpleNegatedParamsShouldContainsNotEqualsAndEquals()
  {
    $this->findAllBy([
      'login not' => 'captain',
      'password' => 'harlock'
    ])
      ->assertSql('select * from `mock_user` where (`login`!=\'captain\') and (`password`=\'harlock\')');
  }


  /** @test */
  public function withArrayParamShouldContainsInStatement()
  {
    $this->findAllBy(['login' => ['captain', 'private']])
      ->assertSql('select * from `mock_user` where (`login` in (\'captain\',\'private\'))');
  }


  /** @test */
  public function withNegatedArrayShouldContainsNotInStatement()
  {
    $this->findAllBy(['name not' => ['Harlock', 'Nausicaa']])
      ->assertSql('select * from `mock_user` where (`name` not in (\'Harlock\',\'Nausicaa\'))');
  }


  /** @test */
  public function withNullValueShouldCountainsIsNullStatement()
  {
    $this->findAllBy(['parent_id' => null])
      ->assertSql('select * from `mock_user` where (`parent_id` is null)');
  }


  /** @test */
  public function withNegatedNullShouldContainsIsNotNullStatement()
  {
    $this->findAllBy(['name not' => null])
      ->assertSql('select * from `mock_user` where (`name` is not null)');
  }


  /** @test */
  public function withLimit2at80OffsetShouldContainsLimitStatement()
  {
    $this->findAllBy(['limit' => [2, 80]])
      ->assertSql('select * from `mock_user` limit 80,2');
  }


  /** @test */
  public function withOrderNameShouldContainsOrderByNameAscStatement()
  {
    $this->findAllBy(['order' => 'name'])
      ->assertSql('select * from `mock_user` order by `name` asc');
  }


  protected function findAllBy($params)
  {
    User::findAllBy($params);
    return $this;
  }
}



class LoaderCountByTest extends LoaderTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    $called = 0;
    $this->_result->whenCalled('fetch_assoc')
      ->willDo(function () use (&$called) {
        $called++;
        if (1 == $called)
          return ['numberOf' => 3];

        return false;
      });
  }


  /** @test */
  public function countByWithWhereShouldBuildRightSQL()
  {
    $this->assertEquals(3, User::countBy(['where' => 'nom like "%zork%"']));
    $this->assertSql('select count(id) as numberOf from `mock_user` where (nom like "%zork%")');
  }
}



class LoaderCreateTest extends LoaderTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    User::create();
  }


  /** @test */
  public function shouldCreateTableMockUser()
  {
    $this->assertStringStartsWith('create table `mock_user`', $this->_queries[0]);
  }


  /** @test */
  public function shouldHaveIdInt11UnsignedNotNullAutoIncrement()
  {
    $this->assertStringContainsString('`id` int(11) unsigned not null auto_increment', $this->_queries[0]);
  }


  /** @test */
  public function shouldHavePrimaryId()
  {
    $this->assertStringContainsString('primary KEY `id` (`id`)', $this->_queries[0]);
  }


  /** @test */
  public function shouldHaveNameVarchar255NotNull()
  {
    $this->assertStringContainsString('`name` varchar(255) not null', $this->_queries[0]);
  }


  /** @test */
  public function shouldHaveAvatarBlob()
  {
    $this->assertStringContainsString('`avatar` longblob null', $this->_queries[0]);
  }


  /** @test */
  public function shouldHaveDescriptionText()
  {
    $this->assertStringContainsString('`description` longtext null', $this->_queries[0]);
  }


  /** @test */
  public function shouldHaveKeyName()
  {
    $this->assertStringContainsString('KEY `name` (`name`)', $this->_queries[0]);
  }


  /** @test */
  public function shouldHaveBirthdayDateNull()
  {
    $this->assertStringContainsString('`birthday` date null', $this->_queries[0]);
  }


  /** @test */
  public function engineShouldBeMyIsam()
  {
    $this->assertStringContainsString('ENGINE=MyISAM', $this->_queries[0]);
  }
}



class LoaderDropTest extends LoaderTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    User::drop();
  }


  /** @test */
  public function shouldDropTableMockUser()
  {
    $this->assertEquals('drop table `mock_user`', $this->_queries[0]);
  }
}
