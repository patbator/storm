<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Model\Loader;

use Storm\Testing\ModelTestCase;
use Storm\Collection;
use Storm\MemoryReport;
use Storm\Model\Collection as ModelCollection;
use Storm\Model\ModelAbstract;
use Storm\Model\Collection\CollectionAbstract;
use Storm\Model\Association\HasOne;


class VolatileUser extends ModelAbstract
{
  protected
    $_table_name = 'users',
    $_table_primary = 'id_user',

    $_default_attribute_values = [
      'level' => '',
      'id_mouth' => null,
      'brain_id' => null
    ],

    $_has_many = ['cats' => [
      'model' => VolatileCat::class,
      'instance_of' => VolatilePets::class,
      'dependents' => 'delete',
      'role' => 'master'
    ]];

  public function isInvite()
  {
    return $this->getLevel() == 'invite';
  }


  public function describeAssociationsOn($associations)
  {
    $associations
      ->add(new HasOne('brain', [
        'model' => VolatileBrain::class,
        'referenced_in' => 'brain_id'
      ]))

      ->add(new HasOne('mouth', [
        'model' => VolatileMouth::class,
        'referenced_in' => 'id_mouth'
      ]));
  }
}



class VolatileBrain extends ModelAbstract
{
  protected
    $_table_name = 'brains',
    $_default_attribute_values = ['weight' => 0];
}



class VolatileMouth extends ModelAbstract
{
  protected
    $_table_name = 'mouths';
}



class VolatileCat extends ModelAbstract
{
  protected
    $_table_name = 'cats',
    $_default_attribute_values = ['name' => ''],

    $_belongs_to = ['master' => ['model' => VolatileUser::class]];
}



class VolatilePets extends CollectionAbstract
{
  public function getNames()
  {
    return (array)$this->collect(function ($c) {
      return $c->getName();
    });
  }
}




class LoaderVolatileTest extends ModelTestCase
{
  public $albert, $hubert, $hubert_brain, $zoe, $max;
  protected $_loader;

  protected function setUp(): void
  {
    parent::setUp();

    VolatileBrain::beVolatile();
    VolatileMouth::beVolatile();


    $this->albert = $this->fixture(
      VolatileUser::class,
      [
        'id' => 1,
        'login' => 'albert',
        'foo' => 'bar',
        'a_start_one_letter_field' => 'value',
        'brain' => (new VolatileBrain())->setWeight(100),
        'mouth' => (new VolatileMouth())
      ]
    );

    $this->hubert = $this->fixture(
      VolatileUser::class,
      [
        'id' => 2,
        'login' => 'hubert',
        'level' => 'invite',
        'foo' => 'snafu',
        'option' => 'set',
        'brain_id' => 2
      ]
    );

    $this->hubert_brain = $this->fixture(
      VolatileBrain::class,
      [
        'id' => 2,
        'weight' => 155
      ]
    );


    $this->zoe = $this->fixture(
      VolatileUser::class,
      [
        'id' => 3,
        'login' => 'zoe',
        'level' => 'admin',
        'foo' => 'snafu',
        'cats' => [
          $this->fixture(
            VolatileCat::class,
            [
              'id' => 3,
              'name' => 'riri'
            ]
          ),

          $this->fixture(
            VolatileCat::class,
            [
              'id' => 4,
              'name' => 'fifi'
            ]
          ),

          $this->fixture(
            VolatileCat::class,
            [
              'id' => 5,
              'name' => 'loulou'
            ]
          )
        ]
      ]
    );

    $this->max = VolatileUser::newInstance([
      'login' => 'max',
      'level' => 'invite',
      'brain_id' => null,
      'id_mouth' => null
    ]);
  }


  /** @test */
  public function albertShouldHaveABrain()
  {
    $this->assertNotNull($this->albert->getBrain());
  }


  /** @test */
  public function mouthWithIdOneShouldHaveBeenSaved()
  {
    $this->assertNotNull(VolatileMouth::find(1));
  }


  /** @test */
  public function albertBrainWeightShouldBe100()
  {
    $this->assertEquals(100, $this->albert->getBrain()->getWeight());
  }


  /** @test */
  public function albertBrainIdShouldBeOne()
  {
    $this->assertEquals(1, $this->albert->getBrainId());
  }


  /** @test */
  public function changingAlbertBrainShouldUpdateItsReferenceInBrainId()
  {
    $better_brain = $this->fixture(VolatileBrain::class, [
      'id' => 3,
      'weight' => 150
    ]);
    $this->albert->setBrain($better_brain);
    $this->assertEquals(3, $this->albert->getBrainId());
  }


  /** @test */
  public function hubertBrainWeightShouldBe155()
  {
    $this->assertEquals(155, $this->hubert->getBrain()->getWeight());
  }


  /** @test */
  public function brainWithIdOneShouldHaveBeenSaved()
  {
    $this->assertEquals(100, VolatileBrain::find(1)->getWeight());
  }


  /** @test */
  public function deletingAlbertShouldDeleteItsBrain()
  {
    $this->albert->delete();
    VolatileBrain::clearCache();
    $this->assertEmpty(VolatileBrain::find(1));
  }


  /** @test */
  public function deletingAlbertWithoutCacheShouldAlsoDeleteItsBrain()
  {
    VolatileBrain::clearCache();
    VolatileUser::clearCache();
    VolatileUser::find(1)->delete();
    $this->assertEmpty(VolatileBrain::find(1));
  }


  /** @test */
  public function removingAlbertBrainShouldNullifyBrainId()
  {
    $this->albert->setBrain(null);
    $this->assertNull($this->albert->getBrainId());
  }


  /** @test */
  public function settingAlbertBrainShouldReturnAlbert()
  {
    $this->assertSame($this->albert, $this->albert->setBrain(null));
  }


  /** @test */
  public function zoeCatsNamesShouldAnswerRiriFifiLoulou()
  {
    $this->assertEquals(
      ['riri', 'fifi', 'loulou'],
      $this->zoe->getCats()->getNames()
    );
  }


  /** @test */
  public function removingRiriFromZoeShouldKillIt()
  {
    $this
      ->zoe
      ->removeCat(VolatileCat::find(3))
      ->save();

    $this->assertNull(VolatileCat::find(3));
  }


  /** @test */
  public function hubertShouldHaveNoCats()
  {
    $this->assertTrue($this->hubert->getCats()->isEmpty());
  }


  /** @test */
  public function firstZoeCatShouldBeRiri()
  {
    $this->assertEquals('riri', $this->zoe->getCats()->first()->getName());
  }


  /** @test */
  public function firstZoeCatsShouldNotBeEmpty()
  {
    $this->assertFalse($this->zoe->getCats()->isEmpty());
  }


  /** @test */
  public function zoeCatsInjectIntoShouldAnswerRiriFifiLoulou()
  {
    $this->assertEquals(
      'riri,fifi,loulou,',
      $this->zoe
        ->getCats()
        ->injectInto(
          '',
          function ($str, $cat) {
            return $str . $cat->getName() . ',';
          }
        )
    );
  }


  /** @test */
  public function zoeCatsCollectIdsShouldAnswerThreeFourFive()
  {
    $this->assertEquals(
      [3, 4, 5],
      (array)$this->zoe->getCats()->collect('id')
    );
  }


  /** @test */
  public function rejectFifiShouldKeepCatsThreeAndFive()
  {
    $this->assertEquals(
      [3, 5],
      (array)$this->zoe
        ->getCats()
        ->reject(function ($c) {
          return $c->getName() == 'fifi';
        })
        ->collect('id')
    );
  }


  /** @test */
  public function selectFourLettersNameShouldKeepCatsThreeAndFour()
  {
    $this->assertEquals(
      [3, 4],
      (array)$this->zoe
        ->getCats()
        ->select(function ($c) {
          return strlen($c->getName()) == 4;
        })
        ->collect('id')
    );
  }


  /** @test */
  public function detectRiriShouldAnswerCatWithIdThree()
  {
    $this->assertEquals(
      3,
      $this->zoe
        ->getCats()
        ->detect(function ($c) {
          return $c->getName() == 'riri';
        })
        ->getId()
    );
  }


  /** @test */
  public function detectWithNoFoundDataShouldAnswerNull()
  {
    $this->assertNull($this->zoe->getCats()->detect(function ($c) {
      return false;
    }));
  }


  /** @test */
  public function useEachDoToUppercaseCatNames()
  {
    $this->zoe
      ->getCats()
      ->eachDo(function ($c) {
        $c->setName(strtoupper($c->getName()));
      });

    $this->assertEquals(
      ['RIRI', 'FIFI', 'LOULOU'],
      $this->zoe->getCats()->getNames()
    );
  }


  /** @test */
  public function useEachDoWithDeleteStringShouldDeleteAllCats()
  {
    $this->zoe->getCats()->eachDo('delete');
    $this->assertCount(0, VolatileCat::findAll());
  }


  /** @test */
  public function reloadingAndSavingZoeShouldNotDeleteCats()
  {
    VolatileUser::clearCache();
    VolatileCat::clearCache();

    $zoe = VolatileUser::find(3);
    $zoe->save();

    VolatileUser::clearCache();
    VolatileCat::clearCache();

    $zoe = VolatileUser::find(3);

    $this->assertEquals(
      [3, 4, 5],
      (array)$zoe->getCats()->collect('id')
    );
  }



  /** @test */
  public function findAllWithNewInstanceWithIdShouldReturnAllUsers()
  {
    $this->assertEquals(
      [$this->albert, $this->hubert, $this->zoe],
      VolatileUser::findAll()
    );
  }


  /** @test */
  public function findAllShouldReturnInstancesThatAreNotNew()
  {
    VolatileUser::clearCache();
    $this->assertFalse(VolatileUser::findAll()[0]->isNew());
  }


  /** @test */
  public function findId2ShouldReturnHubert()
  {
    $this->assertEquals(
      $this->hubert,
      VolatileUser::find(2)
    );
  }


  /** @test */
  public function albertFieldAStartOneLetterFieldAttributeShouldBeInRawAttributes()
  {
    $this->assertContains(
      'a_start_one_letter_field',
      array_keys($this->albert->getRawAttributes())
    );
  }



  /** @test */
  public function findId5ShouldReturnNull()
  {
    $this->assertEquals(
      null,
      VolatileUser::find(5)
    );
  }


  /** @test */
  public function maxSaveShouldSetId()
  {
    $this->max->save();
    $this->assertEquals(4, $this->max->getId());
  }


  /** @test */
  public function findAllWithNewInstanceAndSaveShouldReturnAllUsers()
  {
    $this->max->save();
    $this->assertEquals(
      [$this->albert, $this->hubert, $this->zoe, $this->max],
      VolatileUser::findAll()
    );
  }


  /** @test */
  public function collectionWithHubertAddAllAlbertAndMaxShouldResultInCollectionWithAll()
  {
    $collection = new Collection([$this->hubert]);
    $collection->addAll([$this->albert, $this->max]);
    $this->assertCount(3, $collection);
  }


  /** @test */
  public function findAllInviteShouldReturnMaxEtHubert()
  {
    $this->max->save();
    $this->assertEquals(
      [$this->hubert, $this->max],
      VolatileUser::findAll(['level' => 'invite'])
    );
  }


  /** @test */
  public function selectInvitesBySelectorNameShouldReturnHubert()
  {
    $users = new ModelCollection(VolatileUser::findAll());
    $this->assertEquals(
      [$this->hubert],
      $users->select('isInvite')->getArrayCopy()
    );
  }


  /** @test */
  public function rejectInvitesBySelectorNameShouldReturnAlbertAndZoe()
  {
    $users = new ModelCollection(VolatileUser::findAll());
    $this->assertEquals(
      [$this->albert, $this->zoe],
      $users->reject('isInvite')->getArrayCopy()
    );
  }


  /** @test */
  public function collectAllLoginsShouldReturnHubert()
  {
    $users = new Collection(VolatileUser::findAll());
    $this->assertEquals(
      ['albert', 'hubert', 'zoe'],
      $users->collect(function ($e) {
        return $e->getLogin();
      })->getArrayCopy()
    );
  }


  /** @test */
  public function InviteWithOptionSetShouldReturnHubert()
  {
    $this->max->save();

    $all_users = VolatileUser::findAll([
      'LEVEL' => 'invite',
      'option' => 'set'
    ]);
    $this->assertEquals([$this->hubert], $all_users);
  }


  /** @test */
  public function findAllInviteWithLevelInviteOptionSetUsingScopeShouldReturnHubert()
  {
    $this->max->save();

    $all_users = VolatileUser::findAll(['scope' => [
      'LEVEL' => 'invite',
      'option' => 'set'
    ]]);
    $this->assertEquals([$this->hubert], $all_users);
  }


  /** @test */
  public function selectInviteShouldReturnHubert()
  {
    $all_guest = VolatileUser::findAllBy(['select' => 'isInvite']);
    $this->assertEquals([$this->hubert], array_values($all_guest));
  }


  /** @test */
  public function callbackInviteShouldReturnHubert()
  {
    $all_guest =
      VolatileUser::findAllBy(['callback' => function ($model) {
        return $model->isInvite();
      }]);

    $this->assertEquals([$this->hubert], array_values($all_guest));
  }


  /** @test */
  public function findAllWithLoginHubertAndAlbertSetShouldReturnAlbertAndHubert()
  {
    $this->assertEquals(
      [$this->albert, $this->hubert],
      VolatileUser::findAll(['login' => ['hubert', 'albert']])
    );
  }


  /**  @test */
  public function findAllInviteOrderByLoginNameDescShouldReturnMaxAndHubert()
  {
    $this->max->save();
    $users = VolatileUser::findAllBy([
      'level' => 'invite',
      'order' => 'login desc'
    ]);

    $this->assertSame('max', $users[0]->getLogin());
    $this->assertSame($this->hubert, $users[1]);
  }


  /**  @test */
  public function findAllInviteOrderByUppercasedID_DESCShouldReturnMaxToAlbert()
  {
    $this->max->save();
    $users = VolatileUser::findAllBy(['order' => 'LOGIN DESC']);

    $this->assertSame('zoe', $users[0]->getLogin());
    $this->assertSame('max', $users[1]->getLogin());
    $this->assertSame('hubert', $users[2]->getLogin());
    $this->assertSame('albert', $users[3]->getLogin());
  }


  /**  @test */
  public function findAllOrderByLevelShouldReturnZoeFirst()
  {
    $this->assertEquals(
      [$this->albert, $this->zoe, $this->hubert],
      VolatileUser::findAll(['order' => 'level'])
    );
  }


  /**  @test */
  public function findAllOrderByFooThenIdShouldReturnAlbertZoeHubert()
  {
    $users = VolatileUser::findAll(['order' => 'foo, id']);
    $this->assertEquals(
      ['albert', 'hubert', 'zoe'],
      (new ModelCollection($users))->collect('login')->getArrayCopy()
    );
  }


  /**  @test */
  public function findAllOrderByFooThenIdArrayFormatShouldReturnZoeHubertAlbert()
  {
    $users = VolatileUser::findAll(['order' => ['foo desc', 'id desc']]);
    $this->assertEquals(
      ['zoe', 'hubert', 'albert'],
      (new ModelCollection($users))->collect('login')->getArrayCopy()
    );
  }


  /**  @test */
  public function findAllOrderByFooThenIdDescArrayFormatShouldReturnAlbertZoeHubert()
  {
    $users = VolatileUser::findAll(['order' => ['foo', 'id desc']]);
    $this->assertEquals(
      ['albert', 'zoe', 'hubert'],
      (new ModelCollection($users))->collect('login')->getArrayCopy()
    );
  }


  /**  @test */
  public function findAllOrderByFooDescThenIdDescShouldReturnHubertZoeAlbert()
  {
    $users = VolatileUser::findAll(['order' => 'foo desc, id desc']);
    $this->assertEquals(
      ['zoe', 'hubert', 'albert'],
      (new ModelCollection($users))->collect('login')->getArrayCopy()
    );
  }


  /** @test */
  public function deleteHubertFindAllShouldReturnAlbertAndZoe()
  {
    $this->hubert->delete();
    $this->assertEquals(
      [$this->albert, $this->zoe],
      VolatileUser::findAll()
    );
  }


  /** @test */
  public function deleteHubertFindShouldReturnNull()
  {
    $this->hubert->delete();
    $this->assertNull(VolatileUser::find($this->hubert->getId()));
  }


  /** @test */
  public function countAllShouldReturn3()
  {
    $this->assertEquals(3, VolatileUser::count());
  }


  /** @test */
  public function countByInviteShouldReturn2()
  {
    $this->max->save();
    $this->assertEquals(2, VolatileUser::countBy(['level' => 'invite']));
  }


  /** @test */
  public function limitOneShouldReturnOne()
  {
    $this->assertEquals(1, count(VolatileUser::findAllBy(['limit' => 1])));
  }


  /** @test */
  public function limitOneTwoShouldReturnTwo()
  {
    $this->assertEquals(2, count(VolatileUser::findAllBy(['limit' => '1, 2'])));
  }


  /** @test */
  public function deleteByLevelInviteShouldDeleteHubertAndMax()
  {
    VolatileUser::deleteBy(['level' => 'invite']);
    $this->assertEquals(2, count(VolatileUser::findAll()));
  }


  /** @test */
  public function deleteByLevelInviteShouldRemoveHubertFromCache()
  {
    VolatileUser::deleteBy(['level' => 'invite']);
    $this->assertNull(VolatileUser::find($this->hubert->getId()));
  }


  /** @test */
  public function deleteByShouldPaginateAndDeleteAll()
  {
    VolatileUser::deleteBy([], 1);
    $this->assertEquals(0, count(VolatileUser::findAll()));
  }


  /** @test */
  public function savingAndLoadingFromPersistenceShouldSetId()
  {
    VolatileUser::clearCache();
    $hubert = VolatileUser::find(2);
    $this->assertEquals(2, $hubert->getId());
  }


  /** @test */
  public function limitPageShouldPaginate()
  {
    $this->assertEquals(
      [$this->albert, $this->hubert],
      VolatileUser::findAll(['limitPage' => [0, 2]])
    );
    $this->assertEquals(
      [$this->albert, $this->hubert],
      VolatileUser::findAll(['limitPage' => [1, 2]])
    );
    $this->assertEquals(
      [$this->zoe],
      VolatileUser::findAll(['limitPage' => [2, 2]])
    );
  }


  /** @test */
  public function limitPageAndAttributeShouldWork()
  {
    $this->assertEquals(
      [$this->hubert, $this->zoe],
      VolatileUser::findAllBy(['foo' => 'snafu', 'limitPage' => [1, 2]])
    );
  }


  /** @test */
  public function memoryReportShouldPrintThreeUsers()
  {
    $this->assertStringContainsString(
      VolatileUser::class . ': 3',
      MemoryReport::renderText()
    );
  }
}
