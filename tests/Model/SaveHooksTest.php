<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use StormTest\Mock\User;


abstract class SaveHooksTestCase extends ModelTestCase
{
  protected
    $_user,
    $_mock;

  protected function setUp(): void
  {
    parent::setUp();

    $this->_user = new User();
    $this->_mock = $this->mock();
    $this->_user->setTraceHookMock($this->_mock);
  }
}



class SaveHooksValidTest extends SaveHooksTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    $this->_mock
      ->whenCalled('beforeSave')->answers(true)
      ->whenCalled('afterSave')->answers(true);
    $this->_user->setValid(true)->save();
  }


  /** @test */
  public function shouldCallBeforeSave()
  {
    $this->assertTrue($this->_mock->methodHasBeenCalled('beforeSave'));
  }


  /** @test */
  public function shouldCallAfterSave()
  {
    $this->assertTrue($this->_mock->methodHasBeenCalled('afterSave'));
  }
}



class SaveHooksInvalidTest extends SaveHooksTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    $this->_user->setValid(false)->save();
  }


  /** @test */
  public function shouldNotCallBeforeSave()
  {
    $this->assertFalse($this->_mock->methodHasBeenCalled('beforeSave'));
  }


  /** @test */
  public function shouldNotCallAfterSave()
  {
    $this->assertFalse($this->_mock->methodHasBeenCalled('afterSave'));
  }
}
