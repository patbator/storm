<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use Storm\Testing\ObjectWrapper;
use Storm\Model\Loader\Cache;
use Storm\Model\Loader;
use StormTest\Mock\Dog;


class CacheTest extends ModelTestCase
{
  public $table_dog, $zend_cache;
  protected $_quito_key, $_quito;

  protected function setUp(): void
  {
    parent::setUp();

    $this->table_dog = ObjectWrapper::mock();
    $this->zend_cache = ObjectWrapper::mock();

    $cache = new Cache('my_application', $this->zend_cache);

    Loader::setDefaultCache($cache);

    $dog_loader = Dog::getLoader();
    $dog_loader->setTable($this->table_dog);


    $this->_quito_key = md5(serialize(['my_application', Dog::class, 23]));

    $this->_quito = Dog::newInstance(['id' => 23, 'nom' => 'quito']);
  }


  protected function tearDown(): void
  {
    Loader::resetCache();
    parent::tearDown();
  }


  /** @test */
  public function whenInstanceInCacheShouldGetItFromCache()
  {
    $this->zend_cache
      ->whenCalled('load')
      ->with($this->_quito_key)
      ->answers(serialize($this->_quito));

    $this->assertEquals($this->_quito, Dog::find(23));
  }


  /** @test */
  public function whenInstanceNotInCacheShouldGetItAndCacheIt()
  {
    $this->zend_cache
      ->whenCalled('load')->answers(false)
      ->whenCalled('save')->answers(true);

    Dog::getPersistenceStrategy()
      ->update(['id' => 23, 'nom' => 'quito'], 23);

    $dbQuito = Dog::find(23);

    $this->assertTrue($this->zend_cache
      ->methodHasBeenCalledWithParams(
        'save',
        [serialize($dbQuito), $this->_quito_key]
      ));
  }
}
