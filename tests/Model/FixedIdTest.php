<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2023- Patrick Barroca
Copyright (c) 2010-2015 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use Storm\Model\ModelAbstract;


class FixedIdVolatileTest extends ModelTestCase
{
  protected $_model;

  protected function setUp(): void
  {
    parent::setUp();
    FixedIdModel::beVolatile();

    $this->_model = FixedIdModel::newInstance()
      ->setCode('FIXED_CODE');
    $this->_model->save();
  }


  /** @test */
  public function shouldBeFoundWithFixedId()
  {
    $this->assertNotNull(FixedIdModel::find('FIXED_CODE'));
  }


  /** @test */
  public function modelIdShouldBeFixed()
  {
    $this->assertEquals('FIXED_CODE', $this->_model->getId());
  }
}


class FixedIdDbTest extends ModelTestCase
{
  protected $_model, $_table, $_persistence;

  protected function setUp(): void
  {
    parent::setUp();

    $this->_persistence = $this->mock()
      ->whenCalled('insert')
      ->answers(1);

    FixedIdModel::setPersistenceStrategy($this->_persistence);

    $this->_model = FixedIdModel::newInstance()
      ->setCode('FIXED_CODE');

    $this->_model->save();
  }


  /** @test */
  public function shouldCallInsertWithFixedId()
  {
    $this->assertTrue($this->_persistence
      ->methodHasBeenCalledWithParams(
        'insert',
        [['code' => 'FIXED_CODE']]
      ));
  }
}



class FixedIdModel extends ModelAbstract
{
  protected $_table_primary = 'code';
  protected $_fixed_id = true;
}
