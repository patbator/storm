<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2023- Patrick Barroca
Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


/**
digraph G {
  Model_A -> Model_A [label="parent"];
  Model_A -> Model_B [label="a_to_b"];
  Model_A -> Model_A [label="children"];
  Model_B -> Model_A [label="b_to_a"];
  Model_B -> Model_C [label="b_to_c"];
  Model_C -> Model_B [label="c_to_b"];
}
 */

namespace StormTest\Mock;

use Storm\Model\ModelAbstract;
use Storm\Testing\ModelTestCase;


class GraphTest extends ModelTestCase
{
  public function datas()
  {
    return [
      [(new Model_A())->dotGraph()],
      [(new Model_B())->dotGraph()],
      [(new Model_C())->dotGraph()]
    ];
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldStartWithGraphG($graph)
  {
    $this->assertEquals(0, strpos($graph, 'digraph G {'));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldEndWithGraphG($graph)
  {
    $this->assertEquals('}', substr($graph, -1));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldContainAToAChildrenLink($graph)
  {
    $this->assertTrue(false !== strpos(
      $graph,
      Model_A::class . ' -> ' . Model_A::class . ' [label="children"]'
    ));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldContainAToAParentLink($graph)
  {
    $this->assertTrue(false !== strpos(
      $graph,
      Model_A::class . ' -> ' . Model_A::class . ' [label="parent"]'
    ));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldContainAToBLink($graph)
  {
    $this->assertTrue(false !== strpos(
      $graph,
      Model_A::class . ' -> ' . Model_B::class . ' [label="a_to_b"]'
    ));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldContainBToALink($graph)
  {
    $this->assertTrue(false !== strpos(
      $graph,
      Model_B::class . ' -> ' . Model_A::class
    ));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldContainBToCLink($graph)
  {
    $this->assertTrue(false !== strpos(
      $graph,
      Model_B::class . ' -> ' . Model_C::class
    ));
  }


  /**
   * @dataProvider datas
   * @test
   */
  public function shouldContainCToBLink($graph)
  {
    $this->assertTrue(false !== strpos(
      $graph,
      Model_C::class . ' -> ' . Model_B::class
    ));
  }
}



class Model_A extends ModelAbstract
{
  protected $_belongs_to = ['parent' => ['model' => Model_A::class]];
  protected $_has_many = [
    'a_to_b' => ['model' => Model_B::class],
    'children' => ['model' => Model_A::class]
  ];
}



class Model_B extends ModelAbstract
{
  protected $_belongs_to = ['b_to_a' => ['model' => Model_A::class]];
  protected $_has_many = ['b_to_c' => ['model' => Model_C::class]];
}



class Model_C extends ModelAbstract
{
  protected $_belongs_to = ['c_to_b' => ['model' => Model_B::class]];
}
