<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Model\Association;

use Storm\Testing\ModelTestCase;
use Storm\Model\ModelAbstract;


class Robot extends ModelAbstract
{
  protected $_table_name = 'robot';
  protected $_belongs_to = ['group' => ['model' => RobotGroup::class]];
  protected $_has_many = [
    'energy_cells' => [
      'model' => EnergyCell::class,
      'role' => 'robot',
      'unique' => true,
      'dependents' => 'delete',
      'order' => 'load desc',
      'scope' => ['load not' => 0],
    ],
    'powers' => [
      'through' => 'energy_cells',
    ],
    'tags' => [
      'through' => 'group',
    ],
  ];
}


class EnergyCell extends ModelAbstract
{
  protected $_table_name = 'energy_cell';
  protected $_belongs_to = ['robot' => ['model' => Robot::class]];
  protected $_has_many = ['powers' => [
    'model' => Power::class,
    'role' => 'cell'
  ]];
}


class Power extends ModelAbstract
{
  protected $_table_name = 'power';
  protected $_belongs_to = ['cell' => ['model' => EnergyCell::class]];
}


class RobotGroup extends ModelAbstract
{
  protected $_table_name = 'group';
  protected $_has_many = ['tags' => [
    'model' => Tag::class,
    'role' => 'group'
  ]];
}


class Tag extends ModelAbstract
{
  protected $_table_name = 'tag';
  protected $_belongs_to = ['group' => ['model' => RobotGroup::class]];
}


class HasManyTest extends ModelTestCase
{
  protected $robot;

  protected function setUp(): void
  {
    parent::setUp();

    $cell = EnergyCell::newInstance(
      [
        'load' => 100,
        'powers' => [Power::newInstance(['label' => 'vision'])]
      ]
    );

    $group = $this->fixture(
      RobotGroup::class,
      [
        'id' => 66,
        'tags' => [Tag::newInstance()]
      ]
    );

    $this->robot = $this
      ->fixture(
        Robot::class,
        [
          'id' => 14,
          'energy_cells' => [$cell],
          'group' => $group
        ]
      );
  }


  /** @test */
  public function robotShouldHaveManyEnergyCells()
  {
    $this->assertTrue($this->robot->associationAt('energy_cells')->isHasMany());
  }


  /** @test */
  public function robotShouldHaveEnergyCells()
  {
    $this->assertTrue($this->robot->hasEnergyCells());
  }


  /** @test */
  public function numberOfCellsShouldBe1()
  {
    $this->assertEquals(1, $this->robot->numberOfEnergyCells());
  }


  /** @test */
  public function firstEnergyCellShouldBeFullyLoaded()
  {
    $this->assertEquals(100, $this->robot->getEnergyCells()[0]->getLoad());
  }


  /** @test */
  public function robotShouldHavePowers()
  {
    $this->assertTrue($this->robot->hasPowers());
  }


  /** @test */
  public function numberOfPowersShouldBe1()
  {
    $this->assertEquals(1, $this->robot->numberOfPowers());
  }


  /** @test */
  public function robotShouldHaveTags()
  {
    $this->assertTrue($this->robot->hasTags());
  }


  /** @test */
  public function robotShouldHaveOneTag()
  {
    $this->assertEquals(1, $this->robot->numberOfTags());
  }


  /** @test */
  public function robotDeleteShouldDeleteItsEnergyCells()
  {
    $this->robot->delete();
    $this->assertNull(EnergyCell::find(4));
  }


  /** @test */
  public function addingNullCellShouldDoNothing()
  {
    $this->robot->addEnergyCell(null);
    $this->assertEquals(1, $this->robot->numberOfEnergyCells());
  }


  /** @test */
  public function removingNullCellShouldDoNothing()
  {
    $this->robot->removeEnergyCell(null);
    $this->assertEquals(1, $this->robot->numberOfEnergyCells());
  }


  /** @test */
  public function addingCellShouldIncrementCount()
  {
    $this->robot
      ->addEnergyCell(EnergyCell::newInstance())
      ->assertSave();

    $this->assertEquals(2, $this->robot->numberOfEnergyCells());
  }


  /** @test */
  public function addingPowerShouldCreateIntermediary()
  {
    $this->robot
      ->addPower(Power::newInstance())
      ->assertSave();

    $this->assertEquals(2, $this->robot->numberOfEnergyCells());
    $this->assertEquals(2, $this->robot->numberOfPowers());
  }
}
