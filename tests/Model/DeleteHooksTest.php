<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2023- Patrick Barroca
Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use Storm\Testing\ObjectWrapper;
use StormTest\Mock\User;

abstract class DeleteHooksTestCase extends ModelTestCase
{
  protected User $_user;
  protected ObjectWrapper $_wrapper;

  protected function setUp(): void
  {
    $this->_wrapper = $this->onLoaderOfModel(User::class)
      ->whenCalled('beforeDelete')->answers(null)
      ->whenCalled('afterDelete')->answers(null);

    $this->_user = $this->fixture(User::class, ['id' => 1])
      ->setTraceHookMock($this->_wrapper);
  }
}


class SuccessfulDeleteHooksTest extends DeleteHooksTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    $this->_user->delete();
  }


  /** @test */
  public function shouldHaveCalledBefore()
  {
    $this->assertTrue($this->_wrapper->methodHasBeenCalled('beforeDelete'));
  }


  /** @test */
  public function shouldHaveCalledAfter()
  {
    $this->assertTrue($this->_wrapper->methodHasBeenCalled('afterDelete'));
  }
}


class FailedDeleteHooksTest extends DeleteHooksTestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    $this->_wrapper->whenCalled('delete')->answers(false);
    $this->_user->delete();
  }


  /** @test */
  public function shouldHaveCalledBeforeDelete()
  {
    $this->assertTrue($this->_wrapper->methodHasBeenCalled('beforeDelete'));
  }


  /** @test */
  public function shouldNotHaveCalledAfterDelete()
  {
    $this->assertFalse($this->_wrapper->methodHasBeenCalled('afterDelete'));
  }
}
