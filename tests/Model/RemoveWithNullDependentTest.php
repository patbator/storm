<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Model;

use Storm\Testing\ModelTestCase;
use Storm\Model\ModelAbstract;


class RemoveWithNullDependentTest extends ModelTestCase
{
  /** @test */
  public function shouldRemoveWithoutError()
  {
    RemoveWithNullDependentTest_A::beVolatile();
    RemoveWithNullDependentTest_B::beVolatile();
    RemoveWithNullDependentTest_C::beVolatile();

    $this->fixture(
      RemoveWithNullDependentTest_C::class,
      ['id' => 1]
    );

    $this->fixture(
      RemoveWithNullDependentTest_A::class,
      [
        'id' => 1,
        'bs' => []
      ]
    );

    $this->fixture(
      RemoveWithNullDependentTest_B::class,
      [
        'id' => 1,
        'c' => null,
        'a' => RemoveWithNullDependentTest_A::find(1)
      ]
    );

    $this->fixture(
      RemoveWithNullDependentTest_B::class,
      [
        'id' => 2,
        'c' => RemoveWithNullDependentTest_C::find(1),
        'a' => RemoveWithNullDependentTest_A::find(1)
      ]
    );

    RemoveWithNullDependentTest_A::find(1)
      ->addB(RemoveWithNullDependentTest_B::find(1))
      ->addB(RemoveWithNullDependentTest_B::find(2))
      ->removeC(RemoveWithNullDependentTest_C::find(1));

    $this->assertTrue(true);
  }
}


class RemoveWithNullDependentTest_A extends ModelAbstract
{
  protected $_has_many = [
    'bs' => [
      'model' => RemoveWithNullDependentTest_B::class,
      'role' => 'a',
      'dependents' => 'delete'
    ],
    'cs' => [
      'through' => 'bs',
      'unique' => true
    ]
  ];
}


class RemoveWithNullDependentTest_B extends ModelAbstract
{
  protected $_belongs_to =  [
    'a' => [
      'model' => RemoveWithNullDependentTest_a::class,
      'referenced_in' => 'id_a'
    ],
    'c' => [
      'model' => RemoveWithNullDependentTest_c::class,
      'referenced_in' => 'id_c'
    ]
  ];
}


class RemoveWithNullDependentTest_C extends ModelAbstract
{
  protected $_has_many = [
    'bs' => [
      'model' => RemoveWithNullDependentTest_B::class,
      'role' => 'c',
      'dependents' => 'delete'
    ],
    'as' => [
      'through' => 'bs',
      'unique' => true
    ]
  ];
}
