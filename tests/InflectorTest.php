<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest;

use PHPUnit\Framework\TestCase;
use Storm\Inflector;


abstract class InflectorTestCase extends TestCase
{
  protected function setUp(): void
  {
    parent::setUp();
    Inflector::resetCache();
  }
}



class InflectorPluralizeTest extends InflectorTestCase
{
  public function frensh()
  {
    return [
      ['cheval', 'chevaux'],
      ['Lieu', 'Lieux'],
      ['categorie', 'categories'],
      ['avis', 'avis']
    ];
  }


  public function english()
  {
    return [
      ['item', 'items'],
      ['category', 'categories']
    ];
  }


  /**
   * @test
   * @dataProvider frensh
   */
  public function pluralizeFrenshShouldReturnPlural($singular, $plural)
  {
    $this->assertEquals($plural, Inflector::pluralize($singular));
  }


  /**
   * @test
   * @dataProvider english
   */
  public function pluralizEnglishShouldReturnPlural($singular, $plural)
  {
    $this->assertEquals($plural, Inflector::pluralize($singular));
  }
}




class InflectorSinguralizeFrenchTest extends InflectorTestCase
{
  public function frensh()
  {
    return [
      ['chevaux', 'cheval'],
      ['zork', 'zork'],
      ['categories', 'categorie'],
      ['avis', 'avis']
    ];
  }


  public function english()
  {
    return [
      ['activities', 'activity'],
      ['items', 'item']
    ];
  }


  /**
   * @test
   * @dataProvider frensh
   */
  public function singuralizeFrenshShouldReturnSingular($plural, $singular)
  {
    $this->assertEquals($singular, Inflector::singularize($plural));
  }


  /**
   * @test
   * @dataProvider english
   */
  public function singuralizeEnglishShouldReturnSingular($plural, $singular)
  {
    $this->assertEquals($singular, Inflector::singularize($plural));
  }
}



class InflectorCamelizeFrenchTest extends InflectorTestCase
{
  public function datas()
  {
    return [
      ['NEWS', 'News'],
      ['ID_NEWS', 'IdNews']
    ];
  }

  /**
   * @test
   * @dataProvider datas
   */
  public function camelizeNEWSShouldReturnNews($value, $camelized)
  {
    $this->assertEquals($camelized, Inflector::camelize($value));
  }
}



class InflectorUnderscorizeFrenchTest extends InflectorTestCase
{
  public function datas()
  {
    return [
      ['IdNews', 'id_news'],
      ['News', 'news'],
      ['APost', 'a_post'],
      ['ABoireEtAManger', 'a_boire_et_a_manger']
    ];
  }

  /**
   * @test
   * @dataProvider datas
   */
  public function underscorizeShouldReturnUnderscorized($value, $expected)
  {
    $this->assertEquals($expected, Inflector::underscorize($value));
  }
}
