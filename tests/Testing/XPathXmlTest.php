<?php
/*
  STORM is under the MIT License (MIT)

  Copyright (c) 2023- Patrick Barroca
  Copyright (c) 2010-2011 Agence Française Informatique http://www.afi-sa.fr

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

namespace StormTest\Testing;

use PHPUnit\Framework\TestCase;
use Storm\Testing\XPathXML;
use Storm\Testing\XPathException;


class Storm_Test_XPathXmlTest extends TestCase
{
  protected
    $_xpath,
    $_xml;

  protected function setUp(): void
  {
    parent::setUp();
    $this->_xpath = new XPathXML();
    $this->_xml = '<?xml version="1.0" encoding="UTF-8" ?>
<models>
  <model>Felix</model>
  <model>Henry</model>
</models>';
  }


  /** @test */
  public function shouldHaveModelNode()
  {
    $this->_xpath->assertXpath($this->_xml, '//model');
    $this->assertTrue(true);
  }


  /** @test */
  public function assertMobNodeShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertXpath($this->_xml, '//mob');
  }


  /** @test */
  public function shouldNotHaveMobNode()
  {
    $this->_xpath->assertNotXpath($this->_xml, '//mob');
    $this->assertTrue(true);
  }


  /** @test */
  public function assertNotModelNodeShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertNotXpath($this->_xml, '//model');
  }


  /** @test */
  public function shouldHaveTwoModelNodes()
  {
    $this->_xpath->assertXpathCount($this->_xml, '//model', 2);
    $this->assertTrue(true);
  }


  /** @test */
  public function assertMobNodeCountShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertXpathCount($this->_xml, '//mob', 2);
  }



  /** @test */
  public function shouldHaveVersion1Dot0()
  {
    $this->_xpath->assertXmlVersion($this->_xml, '1.0');
    $this->assertTrue(true);
  }


  /** @test */
  public function assertVersion3Dot6ShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertXmlVersion($this->_xml, '3.6');
  }


  /** @test */
  public function shouldHaveUtf8Encoding()
  {
    $this->_xpath->assertXmlEncoding($this->_xml, 'UTF-8');
    $this->assertTrue(true);
  }


  /** @test */
  public function assertEncodingIsoShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertXmlEncoding($this->_xml, 'ascii');
  }


  /** @test */
  public function shouldContainsFelix()
  {
    $this->_xpath->assertXpathContentContains($this->_xml, '//model', 'Felix');
    $this->assertTrue(true);
  }


  /** @test */
  public function notContainsFelixShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertNotXpathContentContains($this->_xml, '//model', 'Felix');
  }


  /** @test */
  public function containsCalypsoShouldThrow()
  {
    $this->expectException(XPathException::class);
    $this->_xpath->assertXpathContentContains($this->_xml, '//model', 'Calypso');
  }


  /** @test */
  public function shouldNotContainsCalypso()
  {
    $this->_xpath->assertNotXpathContentContains($this->_xml, '//model', 'Calypso');
    $this->assertTrue(true);
  }
}
