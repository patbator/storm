<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2010-2016 Agence Française Informatique http://www.afi-sa.fr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest;

use Storm\Cache\Cache;
use Storm\Cache\VolatileCache;
use PHPUnit\Framework\TestCase;


class CacheTest extends TestCase
{
  protected VolatileCache $_backend;
  protected Cache $_cache;

  protected function setUp(): void
  {
    parent::setUp();

    Cache::setDefaultCache($this->_backend = new VolatileCache());
    Cache::setSeed('banana');

    $this->_cache = (new Cache())
      ->disableSerialization();
  }


  /** @test */
  public function saveMinionsShouldUseKevinSeed()
  {
    $this->_backend->save('kevin', 'banana');
    $this->_cache->save('too much', 'minions');

    $this->assertEquals(
      'too much',
      $this->_backend->load(md5(serialize(['kevin', 'minions'])))
    );
  }


  /** @test */
  public function saveMinionsWithoutSeedShouldUseGeneratedSeed()
  {
    $this->_cache->save('too much', 'minions');

    $generated_seed = $this->_backend->load('banana');
    $this->assertStringStartsWith('banana_', $generated_seed);

    $this->_cache->save('groo', 'master');
    $this->assertEquals(
      'groo',
      $this->_backend->load(md5(serialize([$generated_seed, 'master'])))
    );
  }


  /** @test */
  public function cleanShouldChangeRealSeed()
  {
    $this->_backend->save('kevin', 'banana');
    $this->_cache->save('too much', 'minions');

    $this->_cache->clean();

    $generated_seed = $this->_backend->load('banana');
    $this->assertStringStartsWith('banana_', $generated_seed);

    $this->assertEmpty($this->_cache->load('minions'));
  }


  /** @test */
  public function dashShouldBeReplacedInSeedAsZendCacheDoesNotWorkWithDashSeed()
  {
    Cache::setSeed('ban-ana');

    $this->_cache->save('too much', 'minions');
    $generated_seed = $this->_backend->load('ban_ana');
    $this->assertStringStartsWith('ban_ana_', $generated_seed);
  }
}
