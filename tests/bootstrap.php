<?php
error_reporting(E_ALL);
$current_dir = realpath(dirname(__FILE__));
$include_paths = [
  $current_dir,
  get_include_path()
];

set_include_path(implode(PATH_SEPARATOR, $include_paths));
date_default_timezone_set('Europe/Paris');

require __DIR__ . '/../vendor/autoload.php';
