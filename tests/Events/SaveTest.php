<?php
/*
STORM is under the MIT License (MIT)

Copyright (c) 2021- Patrick Barroca and contributors, see CONTRIBUTORS.txt


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

namespace StormTest\Events;

use Storm\Events;
use Storm\Events\Save;
use Storm\Events\Event;
use Storm\Testing\ModelTestCase;
use StormTest\Mock\Cat;


class SaveTest extends ModelTestCase
{
  protected array $_events;

  protected function setUp(): void
  {
    parent::setUp();
    $this->_events = [];
    Events::getInstance()->register([$this, 'receiveEvent']);
    (new Cat)
      ->setName('Felix')
      ->save();
  }


  protected function tearDown(): void
  {
    Events::setInstance(null);
    parent::tearDown();
  }


  public function receiveEvent(Event $event): void
  {
    $this->_events[] = $event;
  }


  /** @test */
  public function shouldReceiveSaveEvent()
  {
    $this->assertEquals(Save::class, $this->_events[0]::class);
  }


  /** @test */
  public function eventModelShouldBeFelix()
  {
    $this->assertEquals('Felix', $this->_events[0]->model()->getName());
  }
}
