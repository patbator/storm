STORM 2 Documentation
=====================

Welcome to STORM 2 documentation.

Main goals are
  * ease TDD with models without requiring real database access
  * easy integration in legacy code so you can migrate progressively
  * automatic handling of associations (one to one, one to many, many to many) inspired by Smalltalk SandstoneDb and Ruby On Rails ActiveRecord

.. toctree::
   :maxdepth: 2

   overview
