Overview
========

.. graphviz::

   digraph G {
     node [shape=box]
     rankdir=LR
     subgraph cluster_model {
       style=filled
       fillcolor="#7fc97f"
       node [style=filled fillcolor=white]
       label="Models"
       Model; Loader;
     }
     
     subgraph cluster_persistence {
       style=filled
       fillcolor="#beaed4"
       node [style=filled fillcolor=white]
       label="Persistence"
       Table; Strategy;
     }
     
     subgraph cluster_commands {
       style=filled
       fillcolor="#fdc086"
       node [style=filled fillcolor=white]
       label="Commands"
       Command; CommandVisitor;
     }
     
     Model -> Loader -> Table
     Strategy -> CommandVisitor
     CommandVisitor -> Command
     Table -> Strategy
     Table -> Command
   }


Minimal usage
-------------

.. highlight:: sql
               
Suppose you got this table::

  create table my_super_model (
    id int(11) unsigned not null autoincrement,
    label varchar(255) not null,
    PRIMARY id
  );
  
.. highlight:: php

Extend ModelAbstract and set the table name::

  use Storm\Model\ModelAbstract;
   
  class MySuperModel extends ModelAbstract
  {
      protected $_table_name = 'my_super_model';
  }

Then you can use STORM magic :) ::

  $models = MySuperModel::findAllBy(['order' => ['label' => 'desc']]);
  // gives you a collection of MySuperModel objects ordered by label

  $model = MySuperModel::find(3);
  // gives you the MySuperModel of id=3

  $model->getLabel(); // returns the label column
  
  $model->setLabel('Hello world'); // set the label and returns the object so you can chain
  
  $model->save();
  // insert or update the database row
  // returns true on success false on failure

  MySuperModel::newInstance(['label' => 'I am new'])->save();
  // creates a new instance and saves it

Connecting to database
----------------------

By default STORM will use volatile persistence strategy, this means models are read and writen from and in memory.

To persist your model in your DBRMS you will have to tell him to use another strategy.
Only Mysqli is supported at the moment.

First create a configuration object ::

  $config = new Storm\Persistence\Configuration(
    ['host' => 'yourhost',
     'database' => 'yourdatabase',
     'user' => 'user',
     'pass' => 'pass']
  );

Then tell the connection manager to use a connection with this configuration as default ::

  Storm\Persistence\Connections::getInstance()
    ->setDefault(new Storm\Persistence\Mysqli\Connection($config));

And then tell the loader to use Mysqli strategy as default ::

  Storm\Model\Loader::defaultTo(Storm\Persistence\Mysqli\Strategy::class);
